 /*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
function stat5render() {
    $.ajax ({
        url: '/ajax/stat5ajax',
        dataType: 'JSON',
        success: function (resp) {
            $('#total').text(resp.total);
            $('#progress').text(resp.progress);
            $('#success').text(resp.success);
            $('#failed').text(resp.failed);
            $('#captcha').text(resp.captcha);
            $('#amazon404').text(resp.amazon404);
            $('#amazon500').text(resp.amazon500);
            $('#cpu').text(resp.cpu);
            //console.log(resp);
            stat5render();
        }
    })
}

$(document).ready(function () {
    stat5render();
});

