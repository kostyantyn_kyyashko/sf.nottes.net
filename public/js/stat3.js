 /*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
function stat3render() {
    $.ajax ({
        url: '/ajax/stat3ajax',
        dataType: 'JSON',
        success: function (resp) {
            $('#total').text(resp.total);
            $('#progress').text(resp.progress);
            $('#parsed').text(resp.parsed);
            $('#pages').text(resp.pages);
            $('#cpu').text(resp.cpu);
            //console.log(resp);
            stat3render();
        }
    })
}

$(document).ready(function () {
    stat3render();
});

