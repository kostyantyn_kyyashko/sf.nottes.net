<?php
/**
 * get page by asin, get asin's data and get sellerIDs
 * @param GearmanJob $job
 */
function GetSellersByAsin (\GearmanJob $job)
{
    $job = unserialize($job->workload());
    $asin = $job['asin'];
    $page = isset($job['page'])?$job['page']:1;

    $gclient = new \GearmanClient();
    $gclient->addServer(\App\Core\GMonitor::$host, \App\Core\GMonitor::$port);

    $asinToSellersParser = new \App\Core\Parsers\Amazon\AsinToSellersParser();
    $url = $asinToSellersParser->urlGenerate($asin, ($page - 1)*10);

    $proxy = new \App\Document\Items\Proxy();
    $ipProxy = \App\Document\Items\Proxy::randomProxy();
    $proxy->increaseUsage($ipProxy);

    $parser = new \App\Core\Parser();
    $parser->proxy = $ipProxy;
    $html = $parser->webPageGet($url)['content'];
    $sellers = $asinToSellersParser->parseSeller($html);

    //parse asin's page and save data
    if ($page == 1) {
        $asinMini = new \App\Document\Items\Amazon\AsinMini();
        $tmp = $asinMini->getter->findOneBy(['asin' => $asin]);
        if ($tmp && property_exists($tmp, 'title') && strlen($tmp->getTitle()) < 3) {//empty rec in DB. Clear that
            $mb = new \App\Document\MongoBase();
            $mb->remove($tmp);
            $tmp = false;
        }
        if(!$tmp) { //this asin not parsed and not saved in db
            $asinMiniParser = new \App\Core\Parsers\Amazon\AsinMiniParser($parser);
            $uniqData = [
                'asin' => $asin,
            ];
            $asinData = $asinMiniParser->workParseMain($uniqData, $parser, $asinMiniParser);

            //echo "price {$asinData['price']}";

            if (array_key_exists('title', $asinData) && strlen($asinData['title']) > 3) {
                $asinData['asin'] = $asin;
                $asinMini->insertRow($asinData);
            }
        }
    }

    $sellersJsonBuffer = new \App\Document\Items\Amazon\SellersJsonBuffer();

    if ($sellers && is_array($sellers)) {
        $proxy->increaseSuccess($ipProxy);
        $sellersJsonBuffer->insertRow(['sellersJson' => json_encode($sellers)]);
    }
    if(!$asinToSellersParser->isLastPage($html)) {
        $gearman_data =
            [
                'asin' => $asin,
                'page' => $page+1
            ];
        $gclient->doBackground('GetSellersByAsin', serialize($gearman_data));
    }

    unset($gclient);
    return;
}

/**
 * get page by sellerID, get seller's data and get asins
 * @param GearmanJob $job
 */
function GetAsinsBySeller(\GearmanJob $job)
{
    $gclient = new GearmanClient();
    $gclient->addServer(\App\Core\GMonitor::$host, \App\Core\GMonitor::$port);

    $job = unserialize($job->workload());
    $seller = $job['seller'];
    $page = isset($job['page'])?$job['page']:1;
    $sellerToAsinsParser = new \App\Core\Parsers\Amazon\SellerToAsinsParser();
    $postFields = $sellerToAsinsParser->productAjaxDataFields($seller, $page);

    $ajaxUrl = "https://www.amazon.de/sp/ajax/products";

    $proxy = new \App\Document\Items\Proxy();
    $ipProxy = \App\Document\Items\Proxy::randomProxy();
    $proxy->increaseUsage($ipProxy);

    $parser = new \App\Core\Parser();
    $parser->proxy = $ipProxy;
    $resp = $parser->webPageGet($ajaxUrl, '', 1, $postFields);
    $ajax_data = json_decode($resp['content'], 1);

    if (!$ajax_data) { //data not received - put task back to queue
/*        $gearman_data = [
            'seller' => $seller,
            'page' => $page,
        ];
        $gclient->doBackground('GetAsinsBySeller', serialize($gearman_data));*/
    }
    else {

        $asinsJsonBuffer = new \App\Document\Items\Amazon\AsinsJsonBuffer();

        $pageSize = 12;

        $asins = $sellerToAsinsParser->getAsins($ajax_data);


        $total = $sellerToAsinsParser->totalCount($ajax_data);

        if ($asins && is_array($asins) && count($asins) > 0) { //print_r($asins); echo "\n\n$page\n\n";
            $proxy->increaseSuccess($ipProxy);
            $asinsJsonBuffer->insertRow(['asinsJson' => json_encode($asins)]);
        }

        if ($page == 1) {
            $sellerMini = new \App\Document\Items\Amazon\SellerMini();
            $tmp = $sellerMini->getter->findOneBy(['seller' => $seller]);
            if ($tmp && property_exists($tmp, 'info') && strlen($tmp->getInfo()) < 10) {//empty rec in DB. Clear that
                $mb = new \App\Document\MongoBase();
                $mb->remove($tmp);
                $tmp = false;
            }
            if (!$tmp) {
                $sellerMiniParser = new \App\Core\Parsers\Amazon\SellersDataParser($parser);
                $uniq_data = [
                    'seller' => $seller,
                    'marketplaceID' => 'A1PA6795UKMFR9',
                ];
                $sellerData = $sellerMiniParser->workParseMain($uniq_data, $parser, $sellerMiniParser);
                if (array_key_exists('info', $sellerData) && strlen($sellerData['info']) > 10) {//seller no empty
                    $sellerData['seller'] = $seller;
                    $sellerData['totalCount'] = $total;
                    $sellerMini->insertRow($sellerData);
                }
            }
        }

        if ($pageSize * $page < $total) {
            $gearman_data = [
                'seller' => $seller,
                'page' => $page+1,
            ];
            $gclient->doBackground('GetAsinsBySeller', serialize($gearman_data));
        }

        unset($gclient);
        return;
    }
}

function GetAsinsBySellerBigDataPages_Disp_Workers (\GearmanJob $job)
{
    $worker = 'GetAsinsBySellerBigDataPages';
    $workersCount = 50;
    $gm = new \App\Core\GMonitor();
    $delta = $workersCount - $gm->workerCount($worker);
    for ($i = 0; $i < $delta; $i++) {
        $gm::workerStart($worker);
    }

    $gclient = new \GearmanClient();
    $gclient->addServer(\App\Core\GMonitor::$host, \App\Core\GMonitor::$port);
    $gclient->doBackground(__FUNCTION__, ' ');
    unset($gm);
    unset($gclient);
    sleep(mt_rand(1,5));
    return;
}

function GetAsinsBySellerBigDataPages_Disp_Jobs (\GearmanJob $job)
{
    $functionName = 'GetAsinsBySellerBigDataPages';
    $functionsCount = 50;
    $gm = new \App\Core\GMonitor();
    $gclient = new \GearmanClient();
    $gclient->addServer(\App\Core\GMonitor::$host, \App\Core\GMonitor::$port);
    $delta = $functionsCount - $gm->functionStatus($functionName);
    $manager = \App\Document\MongoManager::getInstance()->createManager();
    for ($i = 0; $i < $delta; $i++) {
        $z = $manager->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellersData')
            ->field('asinProcessStatus')->equals(0)
            ->field('totalCount')->notEqual(0)
            ->field('sellerId')->equals(new \MongoRegex('/[A-Z0-9]{10,30}/'))
            ->limit(1)
            ->getQuery()
            ->execute();
        $sellerId = array_values($z->toArray())[0]->getSellerId();
        $gearmanData = [
            'sellerId' => $sellerId,
            'marketplaceId' => 'A1PA6795UKMFR9',
            'page' => 1,
        ];
        $gclient->doBackground('GetAsinsBySellerBigDataPages', serialize($gearmanData));
        //mark seller as in progress
       $manager->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellersData')
           ->updateOne()
            ->field('sellerId')->equals($sellerId)
            ->field('marketplaceId')->equals('A1PA6795UKMFR9')
            ->field('asinProcessStatus')->set(1)
            ->getQuery()
            ->execute();
    }
    $gclient->doBackground(__FUNCTION__, ' ');
    unset($gm);
    unset($gclient);
    return;
}

function GetAsinsBySellerBigDataPages(\GearmanJob $job)
{
    $gclient = new GearmanClient();
    $gclient->addServer(\App\Core\GMonitor::$host, \App\Core\GMonitor::$port);

    $job = unserialize($job->workload());
    $sellerId = $job['sellerId'];
    $page = isset($job['page'])?$job['page']:1;
    $marketplaceId = isset($job['marketplaceId'])?$job['marketplaceId']:'A1PA6795UKMFR9';

    $counter = isset($job['counter'])?$job['counter']:0;
    //count attempts of trying page data
    if ($counter > 5) return;

    $ajaxUrl = "https://www.amazon.de/sp/ajax/products";

    $pageSize = 12;
    echo "$sellerId || $page || $counter\n";

    $proxy = new \App\Document\Items\Proxy();
    $ipProxy = \App\Document\Items\Proxy::randomProxy();
    $parser = new \App\Core\Parser();
    $parser->proxy = $ipProxy;

    //get the count of seller's asins
    $totalCount = isset($job['totalCount'])?$job['totalCount']:false;
    if (!$totalCount) {
        $sellerDataParser = new \App\Core\Parsers\Amazon\SellersDataParser($parser);
        $totalCountUrl = $sellerDataParser->totalCountUrl($sellerId, $marketplaceId);

        $proxy->increaseUsage($ipProxy);
        $html = $parser->webPageGet($totalCountUrl)['content'];
        if ($html && strlen($html) > 1000) $proxy->increaseSuccess($ipProxy);

        $totalCount = $sellerDataParser->parseTotalCount($html, 'von\smehr\sals');
        if (!$totalCount) {
            $totalCount = $sellerDataParser->parseTotalCount($html, 'von');
        }
        $totalCount = str_replace(',', '', $totalCount);
        $totalCount = intval($totalCount);
    }

    $sellerToAsinsParser = new \App\Core\Parsers\Amazon\SellerToAsinsParser();
    $postFields = $sellerToAsinsParser->productAjaxDataFields($sellerId, $page, $pageSize);

    $proxy->increaseUsage($ipProxy);
    $resp = $parser->webPageGet($ajaxUrl, '', 1, $postFields)['content'];
    if ($resp && strlen($resp) > 1000) $proxy->increaseSuccess($ipProxy);

    $ajaxData = json_decode($resp, 1);
    $asins = $sellerToAsinsParser->getAsins($ajaxData);
    //some data not correct - put task back to queue, increase counter
    if (!($ajaxData && $totalCount && $asins && is_array($asins) && count($asins))) {
        $gearmanData = [
            'sellerId' => $sellerId,
            'page' => $page,
            'counter' => $counter+1,
        ];
        $gclient->doBackground(__FUNCTION__, serialize($gearmanData));
        return;
    }
    else {
        $manager = \App\Document\MongoManager::getInstance()->createManager();
        $asinMarketplaceId = new \App\Document\Items\Amazon\BigData\AsinMarkeplaceId();
        //put page num
        $manager
            ->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellersData')
            ->updateOne()
            ->field('sellerId')->equals($sellerId)
            ->field('marketplaceId')->equals($marketplaceId)
            ->field('pages')->push($page)
            ->getQuery()
            ->execute();

        foreach ($asins as $asin) {
            //add asin to seller
            $manager
            ->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellersData')
            ->updateOne()
            ->field('sellerId')->equals($sellerId)
            ->field('marketplaceId')->equals($marketplaceId)
            ->field('asins')->push($asin)
            ->getQuery()
            ->execute();
            //check if asin exists
            $asinDb = $asinMarketplaceId->getter->findOneBy([
                'asin' => $asin,
                'marketplaceId' => $marketplaceId
            ]);

            //no exists - insert to db
            if (!$asinDb) {
                $asinMarketplaceId->insertRow([
                    'asin' => $asin,
                    'marketplaceId' => $marketplaceId,
                    'usage' => 1,
                    'sellerIds' => []
                ]);
            }
            $manager
                ->createQueryBuilder('\App\Document\Items\Amazon\BigData\AsinMarkeplaceId')
                ->updateOne()
                ->field('asin')->equals($asin)
                ->field('marketplaceId')->equals($marketplaceId)
                ->field('usage')->set(1)
                ->field('sellerIds')->push($sellerId)
                ->getQuery()
                ->execute();

            echo $asin . "\n";
        }

        //not last page - put next page to queue
        if ($pageSize * $page <= $totalCount) {
            $gearmanData = [
                'sellerId' => $sellerId,
                'page' => $page+1,
                'totalCount' => $totalCount
            ];
            $gclient->doBackground(__FUNCTION__, serialize($gearmanData));
        }
        //last page - mark seller as handled
        else {
            $manager->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellersData')
                ->updateOne()
                ->field('sellerId')->equals($sellerId)
                ->field('marketplaceId')->equals('A1PA6795UKMFR9')
                ->field('asinProcessStatus')->set(2)
                ->getQuery()
                ->execute();

        }
        unset($gclient);
        return;
    }
}

/**
 * main dispatcher. Check count of other dispatchers and restart it if need
 */
function Dsp ()
{
    $disps = [
        'DispAsins' => 3,
        'DispSellers' => 15,
    ];

    $gm = new \App\Core\GMonitor();

    foreach ($disps as $disp => $count) {
        if (intval($gm->workerCount($disp)) < $count) {
            $gm::workerStart($disp);
        }
    }

    $gclient = new \GearmanClient();
    $gclient->addServer(\App\Core\GMonitor::$host, \App\Core\GMonitor::$port);
    $gclient->doBackground(__FUNCTION__, ' ');
    unset($gm);
    unset($gclient);
    sleep(mt_rand(1,5));
    return;
}

/**
 * asin's dispatcher
 */
function DispAsins ()
{
    $workersCount = 81;
    $gm = new \App\Core\GMonitor();
    $gclient = new \GearmanClient();
    $gclient->addServer(\App\Core\GMonitor::$host, \App\Core\GMonitor::$port);
    $sellerMini = new \App\Document\Items\Amazon\SellerMini();
        $deltaAsins = intval($gm->functionStatus('GetAsinsBySeller'))
            - $workersCount;
        while ($deltaAsins < 0) {
            $deltaAsins = intval($gm->functionStatus('GetAsinsBySeller')) -
                $workersCount;

            $db = new \App\Document\Items\Amazon\SellersJsonBuffer();
            $items = $db->getAndRemoveItems();
            if ($items) {
                foreach ($items as $seller) {
                    $tmp = $sellerMini->getter->findOneBy(['seller' => $seller]);
                    if (!$tmp) { //this seller not parsed and not in DB
                        $gearman_data = [
                            'seller' => $seller,
                            'page' => 1,
                        ];
                        $gclient->doBackground('GetAsinsBySeller', serialize($gearman_data));
                    }
                }
            }
            //usleep(10000);
        }
        while (intval($gm->workerCount('GetAsinsBySeller')) < $workersCount) {
                \App\Core\GMonitor::workerStart('GetAsinsBySeller');
                usleep(10000);
        }
        usleep(mt_rand(100000, 500000));
        $gclient->doBackground(__FUNCTION__, serialize(' '));

        if ($gm->workerCount('Dsp') == 0) {
            \App\Core\GMonitor::workerStart('Dsp');
        }

        unset($gm);
        unset($gclient);
        return;

}


/**
 * seller's dispatcher
 */
function DispSellers ()
{
    $workersCount = 81;
    $gm = new \App\Core\GMonitor();
    $gclient = new \GearmanClient();
    $gclient->addServer(\App\Core\GMonitor::$host, \App\Core\GMonitor::$port);

    $asinMini = new \App\Document\Items\Amazon\AsinMini();

        $deltaSellers = $gm->functionStatus('GetSellersByAsin')
            - $gm->workerCount('GetSellersByAsin');
        while ($deltaSellers < 0) {
            $deltaSellers = $gm->functionStatus('GetSellersByAsin')
                - $gm->workerCount('GetSellersByAsin');

            $db = new \App\Document\Items\Amazon\AsinsJsonBuffer();
            $items = $db->getAndRemoveItems();
            if ($items) {
                foreach ($items as $asin) {
                    $tmp = $asinMini->getter->findOneBy(['asin' => $asin]);
                    if (!$tmp) {
                        $gearman_data = [
                            'asin' => $asin,
                            'page' => 1,
                        ];
                        $gclient->doBackground('GetSellersByAsin', serialize($gearman_data));
                    }
                }
            }
            //usleep(10000);
        }

        while (intval($gm->workerCount('GetSellersByAsin')) < $workersCount) {
                \App\Core\GMonitor::workerStart('GetSellersByAsin');
                usleep(10000);
        }
        usleep(mt_rand(100000, 500000));
        $gclient->doBackground(__FUNCTION__, serialize(' '));

    if ($gm->workerCount('Dsp') == 0) {
        \App\Core\GMonitor::workerStart('Dsp');
    }

    unset($gm);
        unset($gclient);
        return;

}

function DispSellersFullFromBigData ()
{
    $workersCount = 100;
    $jobsCount = 500;
    $gm = new \App\Core\GMonitor();
    $gclient = new \GearmanClient();
    $gclient->addServer(\App\Core\GMonitor::$host, \App\Core\GMonitor::$port);


    $deltaSellers = $gm->functionStatus('SellersFullFromBigData')
        - $jobsCount;
    while ($deltaSellers < 0) {

        $deltaSellers = $gm->functionStatus('SellersFullFromBigData')
            - $jobsCount;

        //get sellerId+markertplaceId from DB
        $sellerIdMarketplaceId = new \App\Document\Items\Amazon\BigData\SellerIdMarkeplaceId();
        $data = $sellerIdMarketplaceId->getter
            ->findOneBy([
                'isParsed' => 0,
                'marketplaceId' => 'A1PA6795UKMFR9',
            ]);
        $sellerId = $data->getSellerId();
        $marketplaceId = $data->getMarketplaceId();

        //mark data 1 (in queue)
        $manager = \App\Document\MongoManager::getInstance()->createManager();
        $manager->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellerIdMarkeplaceId')->findAndUpdate()
            ->field('sellerId')->equals($sellerId)
            ->field('marketplaceId')->equals($marketplaceId)
            ->field('isParsed')->set(1)
            ->getQuery()
            ->execute();

        //put job to queue
        $queueData = [
            'sellerId' => $sellerId,
            'marketplaceId' => $marketplaceId,
        ];
        $gclient->doBackground('SellersFullFromBigData', serialize($queueData));

    }

    while (intval($gm->workerCount('SellersFullFromBigData')) < $workersCount) {
            \App\Core\GMonitor::workerStart('SellersFullFromBigData');
            usleep(10000);
    }
    usleep(mt_rand(100000, 500000));
    $gclient->doBackground(__FUNCTION__, serialize(' '));

    unset($gm);
    unset($gclient);
    unset($manager);
    unset($sellerIdMarketplaceId);
    return;

}

function SellersFullFromBigData (\GearmanJob $job)
{
    $job = unserialize($job->workload());
    $sellerId = $job['sellerId'];
    $marketplaceId = isset($job['marketplaceId'])?$job['marketplaceId']:'A1PA6795UKMFR9';

    $gclient = new \GearmanClient();
    $gclient->addServer(\App\Core\GMonitor::$host, \App\Core\GMonitor::$port);

    $proxy = new \App\Document\Items\Proxy();
    $ipProxy = \App\Document\Items\Proxy::randomProxy();
    $proxy->increaseUsage($ipProxy);

    $parser = new \App\Core\Parser();
    $parser->proxy = $ipProxy;

    $sellerDataParser = new \App\Core\Parsers\Amazon\SellersDataParser($parser);

    $totalCountUrl = $sellerDataParser->totalCountUrl($sellerId, $marketplaceId);
    $html = $parser->webPageGet($totalCountUrl)['content'];
    $totalCount = $sellerDataParser->parseTotalCount($html, 'von\smehr\sals');
    if (!$totalCount) $totalCount = $sellerDataParser->parseTotalCount($html, 'von');
    $totalCount = str_replace(',', '', $totalCount);
    $totalCount = intval($totalCount);

    $uniqData = [
        'sellerId' => $sellerId,
        'marketplaceId' => $marketplaceId,
    ];
    $sellersParsedData = $sellerDataParser->workParseMain($uniqData, $parser, $sellerDataParser);

    echo $sellerId . "\n";

    if (array_key_exists('sellerName', $sellersParsedData) && strlen($sellersParsedData['sellerName']) > 3) {

        $proxy->increaseSuccess($ipProxy);

        $sellersData = new \App\Document\Items\Amazon\BigData\SellersData();
        $sellersParsedData['info'] = $sellerDataParser->clearInfo($sellersParsedData['info']);
        $sellersParsedData['sellerId'] = $sellerId;
        $sellersParsedData['marketplaceId'] = $marketplaceId;
        $sellersParsedData['totalCount'] = $totalCount;
        try {
            $sellersData->insertRow($sellersParsedData);
        }
        catch (\Exception $e) {}


        $manager = \App\Document\MongoManager::getInstance()->createManager();
        $manager->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellerIdMarkeplaceId')->findAndUpdate()
            ->field('sellerId')->equals($sellerId)
            ->field('marketplaceId')->equals($marketplaceId)
            ->field('isParsed')->set(2)
            ->getQuery()
            ->execute();
    }

    unset($parser);
    unset($proxy);
    unset($sellerDataParser);
    unset($sellersData);
    unset($manager);

    return;
}


function Test3 ()
{
    $manager = \App\Document\MongoManager::getInstance()->createManager();
    for ($i = 1; $i<37504; $i++) {
        $pages = $manager
            ->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellersData')
            ->select(['pages'])
            ->field('asinProcessStatus')->equals(1)
            ->limit(1)
            ->getQuery()
            ->execute();
        echo $pages;
    }

}