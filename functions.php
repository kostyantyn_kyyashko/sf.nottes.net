<?php
/******** chain reaction - sellersIds by asin, asins by sellerId *******/

function GetSellersByAsin (\GearmanJob $job)
{
    \App\Core\GearmanFunctions\ChainReaction::getSellersByAsin($job);
    return;
}

function GetAsinsBySeller(\GearmanJob $job)
{
    \App\Core\GearmanFunctions\ChainReaction::getAsinsBySeller($job);
    return;
}

function Dsp ()
{
    \App\Core\GearmanFunctions\ChainReaction::mainDispatcher();
    return;
}

function DispAsins ()
{
    \App\Core\GearmanFunctions\ChainReaction::asinsDispatcher();
    return;
}

function DispSellers ()
{
    \App\Core\GearmanFunctions\ChainReaction::sellersDispatcher();
    return;
}

/*-------------------------- */




/********** parse seller's data by sellerId from bigData dump file ***********/

function DispSellersFullFromBigData ()
{
    \App\Core\GearmanFunctions\SellersFullFromBigData::dispatcher();
    return;
}

function SellersFullFromBigData (\GearmanJob $job)
{
    \App\Core\GearmanFunctions\SellersFullFromBigData::handler($job);
    return;
}

/*---------------------------*/


/********** walk seller's pages and get asins ************/

function AsinsBySellerBigDataPages_Handler(\GearmanJob $job)
{
    \App\Core\GearmanFunctions\AsinsBySellerBigDataPages::Handler($job);
    return;
}

function AsinsBySellerBigDataPages_JobsDispatcher ()
{
    \App\Core\GearmanFunctions\AsinsBySellerBigDataPages::JobsDispatcher();
    return;
}

function AsinsBySellerBigDataPages_WorkersDispatcher (\GearmanJob $job)
{
    \App\Core\GearmanFunctions\AsinsBySellerBigDataPages::WorkersDispatcher();
    return;
}

/* ------------------- */

/********** asins mined from bigData parser ************/

function AsinBigData_Handler(\GearmanJob $job)
{
    \App\Core\GearmanFunctions\AsinBigData::Handler($job);
    return;
}

function AsinBigData_JobsDispatcher ()
{
    \App\Core\GearmanFunctions\AsinBigData::JobsDispatcher();
    return;
}

function AsinBigData_WorkersDispatcher (\GearmanJob $job)
{
    \App\Core\GearmanFunctions\AsinBigData::WorkersDispatcher();
    return;
}

/* ------------------- */

function Test3 ()
{
}