<?php
namespace App\Controller;

use App\Core\GMonitor;
use App\Document\MongoManager;
use App\Document\Utils;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AjaxController
 * @package App\Controller
 */
class AjaxController extends Controller
{
    /**
     * @Route("/ajax/workers_list")
     * @return Response
     */
    public function workersList ()
    {
        $workers = GMonitor::workerCommandsList();
        return new Response(json_encode($workers));
    }

    /**
     * @Route("/ajax/functions_list")
     * @return Response
     *      */
    public function functionsList ()
    {
        return new Response(json_encode(GMonitor::functionsList()));
    }

    /**
     * @Route("/ajax/all_functions_statuses")
     * @return string JSON
     */
    public function functionStatus ()
    {
        $gm = new GMonitor();
        $functions = $gm->allFunctionsStatuses();
        return new Response(json_encode($functions));
    }

    /**
     * @Route("/ajax/worker_count")
     * @return mixed
     */
    public function workerCount ()
    {
        $gm = new GMonitor();
        return new Response($gm->workerCount());
    }

    /**
     * start needed count of workers
     * @Route("/ajax/worker_start/")
     * @return string
     */
    public function workerStart ()
    {
        $request = Request::createFromGlobals();
        $count = $request->get('count', 1);
        $worker = $request->get('worker');
        for ($i=0; $i<$count; $i++) {
            \App\Core\GMonitor::workerStart($worker);
        }
        return new Response('ok');
    }

    /**
     * @Route("/ajax/worker_stop/")
     * @return string
     */
    public function workerStop ()
    {
        $request = Request::createFromGlobals();
        $worker = $request->get('worker');
        $gm = new GMonitor();
        \App\Core\GMonitor::workerStop($worker);
        return new Response('ok');
    }

    /**
     * @Route("/ajax/reset_function_queue")
     * Reset queue for one function
     */
    public function resetFunctionQueue ()
    {
        $request = Request::createFromGlobals();
        $gm = new GMonitor();
        $functionName = $request->get('function_name');
        //$functions_list = $gm->allFunctionsStatuses();
        //if (isset($functions_list[$function_name]) && $functions_list[$function_name]['in_queue'] > 0) {
        //@todo normal check status of function
        if (true) {
            $gm->resetFunctionQueue($functionName);
            return new Response('ok');
        }
        return new Response("empty");
    }

    /**
     * @Route("/ajax/reset_all_queue")
     * Reset queue for all functions in Gearman Job Server
     */
    public function resetAllQueue ()
    {
        //@todo check reset each function
        $gm = new GMonitor();
        $gm->resetAllQueue();
        return new Response('ok');
    }

    /**
     * @Route("/ajax/stop_all_workers")
     * Reset queue for all functions in Gearman Job Server
     */
    public function stopAllWorkers ()
    {
        //@todo check reset each function
        $allWorkers = GMonitor::workerCommandsList();
        foreach ($allWorkers as $worker) {
            \App\Core\GMonitor::workerStop($worker);
        }
        return new Response('ok');
    }

    /**
     * @Route("/ajax/stat3ajax")
     * @todo INCORRECT!
     */
    public function stat3ajax ()
    {
        $total = MongoManager::getInstance()->createManager()
            ->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellersData')
            ->field('totalCount')->notEqual(0)
            ->getQuery()
            ->execute()
            ->count();
        $progress = MongoManager::getInstance()->createManager()
            ->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellersData')
            ->field('asinProcessStatus')->equals(1)
            ->getQuery()
            ->execute()
            ->count();
/*        $pagesCollection = MongoManager::getInstance()->createManager()
            ->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellersData')
            ->field('asinProcessStatus')->notEqual(0)
            ->getQuery()
            ->execute();
        $pages = 0;
        foreach ($pagesCollection as $item) {
            $pages += round(count($item->getAsins())/12);
        }*/
        $cpu = Utils::getServerLoad();

        return $this->json( [
        ]);

    }

    /**
     * @Route("/ajax/stat5ajax")
     */
    public function stat5ajax ()
    {
        $total = MongoManager::getInstance()->createManager()
            ->createQueryBuilder('\App\Document\Items\Amazon\BigData\AsinMarkeplaceId')
            ->field('usage')->equals(1)
            ->getQuery()
            ->execute()
            ->count();
        $progress = MongoManager::getInstance()->createManager()
            ->createQueryBuilder('\App\Document\Items\Amazon\BigData\AsinMarkeplaceId')
            ->field('processStatus')->equals(1)
            ->getQuery()
            ->execute()
            ->count();
        $success = MongoManager::getInstance()->createManager()
            ->createQueryBuilder('\App\Document\Items\Amazon\BigData\AsinMarkeplaceId')
            ->field('processStatus')->equals(2)
            ->getQuery()
            ->execute()
            ->count();
        $failed = MongoManager::getInstance()->createManager()
            ->createQueryBuilder('\App\Document\Items\Amazon\BigData\AsinMarkeplaceId')
            ->field('processStatus')->equals(3)
            ->getQuery()
            ->execute()
            ->count();
        $captcha = MongoManager::getInstance()->createManager()
            ->createQueryBuilder('\App\Document\Items\Amazon\BigData\AsinMarkeplaceId')
            ->field('processStatus')->equals(6)
            ->getQuery()
            ->execute()
            ->count();
        $amazon404 = MongoManager::getInstance()->createManager()
            ->createQueryBuilder('\App\Document\Items\Amazon\BigData\AsinMarkeplaceId')
            ->field('processStatus')->equals(404)
            ->getQuery()
            ->execute()
            ->count();
        $amazon500 = MongoManager::getInstance()->createManager()
            ->createQueryBuilder('\App\Document\Items\Amazon\BigData\AsinMarkeplaceId')
            ->field('processStatus')->equals(500)
            ->getQuery()
            ->execute()
            ->count();
        $cpu = Utils::getServerLoad();

        return $this->json( [
            'total' => $total,
            'progress' => $progress,
            'success' => $success,
            'failed' => $failed,
            'captcha' => $captcha,
            'amazon404' => $amazon404,
            'amazon500' => $amazon500,
            'cpu' => $cpu,
        ]);

    }

}

