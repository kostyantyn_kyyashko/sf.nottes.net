<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Controller;

use App\Core\GMonitor;
use App\Core\Parser;
use App\Core\Parsers\Amazon\SellersDataParser;
use App\Document\Items\Amazon\BigData\SellersData;
use App\Document\Items\Proxy;
use App\Document\MongoBase;
use App\Document\MongoManager;
use App\Document\Utils;
use Doctrine\MongoDB\Tests\RetryTest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class GmonitorController
 * @package App\Controller
 */
class GmonitorController extends Controller
{
    /**
     * @Route("/gmonitor")
     * @return Response
     */
    public function mainPage ()
    {
        if (!Utils::authByKey()) {
            //return $this->redirect('/auth');
        }
        return $this->render('gmonitor/index.html.twig');
    }

    /**
     * init process
     * @Route("/zero")
     */
    public function zeroStart ()
    {
        $gclient = new \GearmanClient();
        $gclient->addServer('127.0.0.1');
        $data = serialize(['asin' => 'B00NVMIO02']);
        $gclient->doBackground('GetSellersByAsin', $data);
        return new Response('');
    }


    /**
     * @Route("/auth")
     * @return Response
     */
    public function authNeed ()
    {
        return new Response('<h1>Auth need!</h1>');
    }


}
