<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Controller;

use App\Document\Items\Proxy;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProxyController
 * @package App\Controller
 */
class ProxyController extends Controller
{
    /**
     * @Route("/proxy/insert")
     * @return Response
     */
    public function insertNewProxies ()
    {
        $proxy = new Proxy();
        $proxies = $proxy->getter->findAll();

        foreach ($proxies as $proxy1) {
            $proxy->remove($proxy1);
        }

        //proxies by .txt file from fineproxy.de
        $content = file_get_contents('/var/www/spider.nottes.net/sf.nottes.net/src/_outerData/proxy.txt');
//        $content = file_get_contents('/var/www/spider.nottes.net/sf.nottes.net/src/_outerData/proxy20.txt');
//        $content = file_get_contents('/var/www/spider.nottes.net/sf.nottes.net/src/_outerData/proxy3.txt');
        $proxies = explode(PHP_EOL, $content);
        foreach ($proxies as $ip) {
            if (strlen($ip) > 10) {
                $proxy2 = new Proxy();
                $proxy2->setIp(trim($ip));
                $proxy->manager->persist($proxy2);
                unset($proxy2);
                $proxy->manager->flush();
            }
        }


        return new Response('ok');
    }

    /**
     * @Route("/proxy/view")
     */
    public function proxyTable ()
    {
        $proxy = new Proxy();
        $proxies = $proxy->getter->findAll();
        $out2 = [];
        foreach ($proxies as $proxy) {
            $out['ip'] = $proxy->getIp();
            $out['usage'] = $proxy->getUsage();
            $out['success'] = $proxy->getSuccess();
            $out['captcha'] = $proxy->getCaptcha();
            $out['successP'] = $proxy->getUsage()?round($proxy->getSuccess()/$proxy->getUsage()*100):'-';
            $out['captchaP'] = $proxy->getUsage()?round($proxy->getCaptcha()/$proxy->getUsage()*100):'-';

            $out2[] = $out;
            unset($out);
        }
        return $this->render('proxy/proxy.html.twig', ['proxies' => $out2]);
    }

    /**
     * @Route("/proxy/random")
     * @return Response
     *
     */
    public function testRandom ()
    {
        $random = Proxy::randomProxy();
        return new Response($random);
    }

    /**
     * @Route("/proxy/testq")
     * @return Response
     *
     */
    public function testq ()
    {
        $proxy = new Proxy();
        $p = $proxy->getter->findOneBy([]);
        var_dump($p->getIp());
        return new Response('');
    }


}
