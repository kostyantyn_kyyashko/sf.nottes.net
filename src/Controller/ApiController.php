<?php
namespace App\Controller;

use App\Core\GMonitor;
use App\Core\Parser;
use App\Document\MongoBase;
use App\Document\MongoManager;
use App\Document\Utils;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AjaxController
 * @package App\Controller
 */
class ApiController extends Controller
{
    /**
     * @Route("/api/info")
     * @return Response
     */
    public function info ()
    {
        return $this->render('api/info.html.twig');
    }

    /**
     * @Route("/api/sellerData")
     * @return Response
     */
    public function sellerData (Request $request)
    {
        $sellerId = $request->get('sellerId');

        $manager = \App\Document\MongoManager::getInstance()->createManager();
        $data = $manager->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellersData')
            ->hydrate(false)
            ->field('marketplaceId')->equals('A1PA6795UKMFR9')
            ->field('sellerId')->equals($sellerId)
            /*            ->field('isParsed')->equals(2)
                        ->field('totalCount')->notEqual(0)*/
            ->getQuery()
            ->execute();
        $sellerData = $data->getNext();
        unset($sellerData['_id']);
        $asins = $sellerData['asins'];
        $data2 = $manager->createQueryBuilder('\App\Document\Items\Amazon\BigData\AsinMarkeplaceId')
            ->hydrate(false)
            ->field('marketplaceId')->equals('A1PA6795UKMFR9')
            ->field('asin')->in($asins)
            ->field('processStatus')->equals(2)
            /*            ->field('isParsed')->equals(2)
                        ->field('totalCount')->notEqual(0)*/
            ->getQuery()
            ->execute();
        $asinsData = [];
        foreach ($data2 as $a) {
            $asinsData[$a['asin']] = $a;
        }
        $out = [];
        $view404 = $request->get('view404');
        foreach ($asins as $asin) {
            if (array_key_exists($asin, $asinsData)) {
                unset($asinsData[$asin]['_id']);
                unset($asinsData[$asin]['sellerIds']);
                unset($asinsData[$asin]['_id']);
                $out[$asin] = $asinsData[$asin];
            }
            elseif ($view404) {
                $out[$asin] = 404;
            }
        }
        $sellerData['asins'] = $out;
        $sellerData['asinsCount'] = count($out);
        unset($sellerData['pages']);
        Utils::debugView($sellerData,1);
        return $this->json($out[0]);
    }

    /**
     * @Route("/api/getWebPage")
     * @return Response
     */
    public function getWebPage (Request $request)
    {
        $url = $request->get('url');
        $url = urldecode($url);
        $key = $request->get('key');
        if ($key != 'mfo304988kN') die();
        $proxy = new \App\Document\Items\Proxy();
        $ipProxy = $proxy::randomProxy();
        $parser = new Parser();
        $parser->proxy = $ipProxy;
        $html = $parser->webPageGet($url)['content'];

        return new Response($html);
    }


    /**
     * All
     * @Route("/api/sellerIds")
     * @return Response
     */
    public function sellerIds (Request $request)
    {
        $gte = $request->get('gte', 1);
        $lte = $request->get('lte', 1000000000);
        $manager = \App\Document\MongoManager::getInstance()->createManager();
        $query = $manager->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellersData')
            ->hydrate(false)
            ->field('marketplaceId')->equals('A1PA6795UKMFR9')
            ->field('totalCount')->gte(intval($gte))
            ->field('totalCount')->lte(intval($lte));
        $status = intval($request->get('status'));
        $status = ($status && in_array($status, [0,1,2,3,6,404,500]))?$status:2;
        $query->field('asinProcessStatus')->equals($status);
        $sort = $request->get('sort');
        if ($sort && in_array($sort, ['asc', 'desc'])) {
            $query->sort(['totalCount' => $sort]);
        }
        $limit = $request->get('limit', 1000);
        if ($limit) {
            $query->limit(intval($limit));
        }
        $data = $query
            ->getQuery()
            ->execute();
        $out = [];
        foreach ($data as $item) {
            $sellerId = $item['sellerId'];
            $parsedAsinsCount = count($item['asins']);
            $out[$sellerId]['totalCount'] = $item['totalCount'];
            $out[$sellerId]['parsedAsinsCount'] = count($item['asins']);
            $out[$sellerId]['status'] = $item['asinProcessStatus'];
        }
        Utils::debugView($out, 1);
        return $this->json($out);
    }


    /**
     * @Route("/api/updateAsinData")
     * @return Response
     */
    public function updateAsinData (Request $request)
    {
        $asinData = $request->get('asinData');
        $key = $request->get('key');
        //if ($key != 'mfo304988kN') die();
        $manager = MongoManager::getInstance()->createManager();
        $manager->createQueryBuilder('\App\Document\Items\Amazon\BigData\AsinMarkeplaceId')
            ->updateOne()
            ->field('processStatus')->set(intval($request->get('processStatus')))
            ->field('title')->set($request->get('title'))
            ->field('price')->set($request->get('price'))
            ->field('qtyOffers')->set($request->get('qtyOffers'))
            ->field('asin')->equals($request->get('asin'))
            ->field('marketplaceId')->equals($request->get('marketplaceId'))
            ->getQuery()
            ->execute();
        return new Response($request->get('processStatus'));
    }

    /**
     * @Route("/api/getAsins")
     * @return Response
     */
    public function getAsins (Request $request)
    {
        $delta = $request->get('delta');
        $manager = \App\Document\MongoManager::getInstance()->createManager();
        $asins = [];
        $z = $manager->createQueryBuilder('\App\Document\Items\Amazon\BigData\AsinMarkeplaceId')
            ->field('processStatus')->equals(0)
            ->field('usage')->equals(1)
            ->limit($delta)
            ->getQuery()
            ->execute();
        //two generation of parsing

        if ($z->count() == 0) {
            $manager
                ->createQueryBuilder('\App\Document\Items\Amazon\BigData\AsinMarkeplaceId')
                ->updateMany()
                ->field('processStatus')->in([3,1,404,500,6])
                ->field('processStatus')->set(0)
                ->getQuery()
                ->execute();
        }
        $z = $manager->createQueryBuilder('\App\Document\Items\Amazon\BigData\AsinMarkeplaceId')
            ->field('processStatus')->equals(0)
            ->field('usage')->equals(1)
            ->limit($delta)
            ->getQuery()
            ->execute();
        foreach ($z as $c) {
            $asins[] = $c->getAsin();
        }

        foreach ($asins as $asin) {
            //mark asin as in progress
            $manager->createQueryBuilder('\App\Document\Items\Amazon\BigData\AsinMarkeplaceId')
                ->updateOne()
                ->field('asin')->equals($asin)
                ->field('marketplaceId')->equals('A1PA6795UKMFR9')
                ->field('processStatus')->set(1)
                ->getQuery()
                ->execute();
        }

        return $this->json($asins);
    }





}

