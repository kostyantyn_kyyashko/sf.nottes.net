<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Controller;

use App\Document\Items\Amazon\AsinMini;
use App\Document\Items\Amazon\BigData\SellerIdMarkeplaceId;
use App\Document\Items\Amazon\BigData\SellersData;
use App\Document\Items\Amazon\SellerMini;
use App\Document\MongoBase;
use App\Document\MongoManager;
use App\Document\Utils;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PageController
 * @package App\Controller
 */
class PageController extends Controller
{
    /**
     * @Route("/page/asins")
     * @return Response
     */
    public function asins ()
    {
        $asins = new AsinMini();
        $count = $asins->getter->createQueryBuilder()->getQuery()->execute()->count();
        $items = $asins->getter->createQueryBuilder()
            ->field('price')->equals(new \MongoRegex('/EUR*/'))
            ->find()
            ->limit(1000)->getQuery()->execute();
        return $this->render('page/asin.html.twig', ['items' => $items, 'count' => $count]);
    }

    /**
     * @Route("/page/sellers")
     * @return Response
     */
    public function sellers ()
    {
        $seller = new SellersData();
        $count = $seller->getter->createQueryBuilder()->getQuery()->execute()->count();
        $items = $seller->getter->createQueryBuilder()->limit(100)->getQuery()->execute();
        $out = [];
        $out2 = [];
        foreach ($items as $i) {
            $out['seller'] = $i->getSeller();
            $out['totalCount'] = $i->getTotalCount();
            $out['brand'] = $i->getBrand();
            $out['reviewPositive'] = $i->getReviewPositive();
            $out['reviewCount'] = $i->getReviewCount();
            $out['info'] = '<p>' . strip_tags($i->getInfo(), '<br>') . '</p>';
            $out['detailed'] = strip_tags($i->getDetailed(), '<br>');
            $out2[] = $out;
        }
        return $this->render('page/seller.html.twig', ['items' => $out2, 'count' => $count]);
    }

    /**
     * @Route("/page/sellers2")
     * @return Response
     */
    public function sellers2 ()
    {
        $manager = \App\Document\MongoManager::getInstance()->createManager();
        $success = $manager->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellerIdMarkeplaceId')->find()
            ->field('isParsed')->equals(2)
            ->getQuery()
            ->execute()
            ->count();

        $fail = $manager->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellerIdMarkeplaceId')->find()
            ->field('isParsed')->equals(1)
            ->getQuery()
            ->execute()
            ->count();

        $total = $manager->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellerIdMarkeplaceId')->find()
            ->field('marketplaceId')->equals('A1PA6795UKMFR9')
            ->getQuery()
            ->execute()
            ->count();

        if ( !Utils::authByKey()) {
            return $this->redirect('/auth');
        }
        $seller = new SellersData();
        $count = $seller->getter->createQueryBuilder()->getQuery()->execute()->count();
        $items = $seller->getter->createQueryBuilder()->limit(20)->getQuery()->execute();
        $out = [];
        $out2 = [];
        foreach ($items as $i) {
            $out['sellerId'] = $i->getSellerId();
            $out['totalCount'] = $i->getTotalCount();
            $out['sellerName'] = $i->getSellerName();
            $out['reviewPositive'] = $i->getReviewPositive();
            $out['reviewCount'] = $i->getReviewCount();
            $out['info'] = $i->getInfo();
            $out['impressum'] = $i->getImpressum();
            $out['reviewBlock'] = $i->getReviewBlock();
            $out2[] = $out;
        }
        return $this->render('page/seller.html.twig',
            [
                'items' => $out2,
                'count' => $count,
                'success' => $success,
                'fail' => $fail,
                'total' => $total,
            ]);
    }

    /**
     * @Route("/page/stat3")
     * @return Response
     */
    public function stat3 ()
    {
        $total = MongoManager::getInstance()->createManager()
            ->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellersData')
            ->field('totalCount')->notEqual(0)
            ->getQuery()
            ->execute()
            ->count();
        $progress = MongoManager::getInstance()->createManager()
            ->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellersData')
            ->field('asinProcessStatus')->equals(1)
            ->getQuery()
            ->execute()
            ->count();
        $parsed = MongoManager::getInstance()->createManager()
            ->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellersData')
            ->field('asinProcessStatus')->equals(2)
            ->getQuery()
            ->execute()
            ->count();
/*        $pagesCollection = MongoManager::getInstance()->createManager()
            ->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellersData')
            ->field('asinProcessStatus')->notEqual(0)
            ->getQuery()
            ->execute();
        $pages = 0;
        foreach ($pagesCollection as $item) {
            $pages += round(count($item->getAsins())/12);
        }*/
        $cpu = Utils::getServerLoad();

        return $this->render('page/stat3.html.twig', [
            'total' => $total,
            'progress' => $progress,
            'parsed' => $parsed,
            'pages' => 0,
            'cpu' => $cpu,
        ]);
    }

    /**
     * @Route("/page/stat5")
     * @return Response
     */
    public function stat5 ()
    {
        $total = MongoManager::getInstance()->createManager()
            ->createQueryBuilder('\App\Document\Items\Amazon\BigData\AsinMarkeplaceId')
            ->field('usage')->equals(1)
            ->getQuery()
            ->execute()
            ->count();
        $progress = MongoManager::getInstance()->createManager()
            ->createQueryBuilder('\App\Document\Items\Amazon\BigData\AsinMarkeplaceId')
            ->field('processStatus')->equals(1)
            ->getQuery()
            ->execute()
            ->count();
        $success = MongoManager::getInstance()->createManager()
            ->createQueryBuilder('\App\Document\Items\Amazon\BigData\AsinMarkeplaceId')
            ->field('processStatus')->equals(2)
            ->getQuery()
            ->execute()
            ->count();
        $failed = MongoManager::getInstance()->createManager()
            ->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellersData')
            ->field('asinProcessStatus')->equals(3)
            ->getQuery()
            ->execute()
            ->count();
        $captcha = MongoManager::getInstance()->createManager()
            ->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellersData')
            ->field('asinProcessStatus')->equals(6)
            ->getQuery()
            ->execute()
            ->count();
        $amazon404 = MongoManager::getInstance()->createManager()
            ->createQueryBuilder('\App\Document\Items\Amazon\BigData\AsinMarkeplaceId')
            ->field('processStatus')->equals(404)
            ->getQuery()
            ->execute()
            ->count();
        $amazon500 = MongoManager::getInstance()->createManager()
            ->createQueryBuilder('\App\Document\Items\Amazon\BigData\AsinMarkeplaceId')
            ->field('processStatus')->equals(500)
            ->getQuery()
            ->execute()
            ->count();
        $cpu = Utils::getServerLoad();

        return $this->render('page/stat5.html.twig', [
            'total' => $total,
            'progress' => $progress,
            'success' => $success,
            'failed' => $failed,
            'captcha' => $captcha,
            'amazon404' => $amazon404,
            'amazon500' => $amazon500,
            'cpu' => $cpu,
        ]);
    }
}
