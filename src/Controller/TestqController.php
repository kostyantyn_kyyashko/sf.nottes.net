<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Controller;

use App\Core\GMonitor;
use App\Core\Parser;
use App\Core\Parsers\Amazon\SellersDataParser;
use App\Core\Phantom;
use App\Document\Items\Amazon\BigData\SellersData;
use App\Document\Items\Proxy;
use App\Document\MongoBase;
use App\Document\MongoManager;
use App\Document\Utils;
use Doctrine\MongoDB\Tests\RetryTest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class TestqController extends Controller
{
    /**
     * @Route("/testq")
     * @return Response
     */
    public function testq ()
    {
        $url = \App\Core\Parsers\Amazon\SellerFullParser_Prototype::urlForTotalCount('AOK8QVGQLUWZJ');
        $parser = new Parser();
        $parser->proxy = '46.243.173.174:8085';
        $html = $parser->webPageGet($url)['content'];
        $pattern = "/\d{1}\-{0,1}\d{0,3}\svon\s\d{1,6}/";
        preg_match($pattern, $html, $z); var_dump($z);
        if (count($z) > 0) {
            $out = explode(' ', $z[0]);
            $out = intval(trim($out[2]));
        }
        else {
            $out = 0;
        }

        return new Response($out);
    }

    /**
     * @Route("/testq3")
     * @return Response
     */
    public function testq3 ()
    {
        $sellerIdMarketplaceId = new \App\Document\Items\Amazon\BigData\SellerIdMarkeplaceId();
        $data = $sellerIdMarketplaceId->getter->findOneBy(['isParsed' => 0]);
        $sellerId = $data->getSellerId();
        echo '<pre>';
        var_dump($data);
        return new Response($sellerId);
    }


    /**
     * @Route("/testq2")
     * @return Response
     */
    public function testq2 ()
    {
        $manager = MongoManager::getInstance()->createManager();
        $z = $manager->createQueryBuilder('\App\Document\Items\Proxy')->select('test2')
            ->field('ip')->in(['95.85.71.82:8085','185.251.70.251:8085'])
            //->field('test2')->(1)
            ->getQuery()
            ->execute();
        //$f = new \Doctrine\ODM\MongoDB\Cursor();

        var_dump(array_values($z->toArray())[0]->getTest2());
        return new Response('');
    }

    /**
     * @Route("/testq5")
     * @return Response
     */
    public function testq5 ()
    {
        $manager = \App\Document\MongoManager::getInstance()->createManager();
        $manager->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellerIdMarkeplaceId')->updateMany()
            ->field('isParsed')->equals(1)
            ->field('isParsed')->set(0)
            ->getQuery()
            ->execute();
        return new Response('');
    }

    /**
     * @Route("/testq7")
     * @return Response
     */
    public function testq7 ()
    {
        $seller = 'A1VZW46UOC1LOU';
        $sellerToAsinsParser = new \App\Core\Parsers\Amazon\SellerToAsinsParser();
        $postFields = $sellerToAsinsParser->productAjaxDataFields($seller, 1);

        $ajaxUrl = "https://www.amazon.de/sp/ajax/products";

        $proxy = new \App\Document\Items\Proxy();
        $ipProxy = \App\Document\Items\Proxy::randomProxy();
        $proxy->increaseUsage($ipProxy);

        $parser = new \App\Core\Parser();
        $parser->proxy = $ipProxy;
        $resp = $parser->webPageGet($ajaxUrl, '', 1, $postFields);
        $ajax_data = json_decode($resp['content'], 1);
        echo '<pre>';
        print_r($ajax_data);
        return new Response('');
    }


    /**
     * @Route("/testq4")
     * @return Response
     */
    public function testq4 ()
    {
        MongoManager::getInstance()->createManager()
            ->createQueryBuilder('\App\Document\Items\Amazon\BigData\AsinMarkeplaceId')
            ->updateMany()
            ->field('processStatus')->in([3,1,404,6,500])
            ->field('processStatus')->set(0)
            ->getQuery()
            ->execute();
        return new Response('ok');
    }

    /**
     * @Route("/test5")
     * @return Response
     */
    public function test5 ()
    {
        $z = 'Tut uns leid';
        $parser = new Parser();
        $parser->proxy = Proxy::randomProxy();
        $html = $parser->webPageGet('https://www.amazon.de/any/dp/B00S7VXBA0/')['content'];
        var_dump(stristr($html, $z)?1:0);

        echo $html;
        return new Response('');
    }

    /**
     * @Route("/reset7")
     * @return Response
     */
    public function reset7 ()
    {
        return;
        MongoManager::getInstance()->createManager()
            ->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellersData')
            ->updateMany()
            ->field('pages')->set([])
            ->field('asins')->set([])
            ->field('asinProcessStatus')->set(0)
            ->getQuery()
            ->execute();

        MongoManager::getInstance()->createManager()
            ->createQueryBuilder('\App\Document\Items\Amazon\BigData\AsinMarkeplaceId')
            ->updateMany()
            ->field('usage')->set(0)
            ->field('sellerIds')->set([])
            ->getQuery()
            ->execute();

        return new Response('ok');
    }


    /**
     * @Route("/testq9")
     * @return Response
     */
    public function test ()
    {
        $asinData = [
            'asin' => 'B000LR224O',
            'marketplaceId' => 'A1PA6795UKMFR9',
            'title' => 'test',
            'price' => '4.3 ee',
            'qtyOffers' => 31,
        ];
        $asinData['processStatus'] = 5;

        $key = 'mfo304988kN';
        $url = "https://spider.sellerlogic.com/api/updateAsinData?key=$key";
        $parser = new Parser();
        $r = $parser->webPageGet($url, '', 1, http_build_query($asinData));
        return new Response($r['content']);
    }

    /**
     * @Route("/test99")
     * @return Response
     */
    public function tt ()
    {
        $html = <<<html
<div id="pagn" class="pagnHy">
            <span class="pagnLA1"> <span class="srSprite firstPageLeftArrow"></span>
                    <span id="pagnPrevString">Vorherige Seite</span></span>
                <span class="pagnCur">1</span>
                            <span class="pagnLink"><a href="/s/ref=sr_pg_2?me=A1VZW46UOC1LOU&amp;rh=i%3Amerchant-items&amp;page=2&amp;ie=UTF8&amp;qid=1528764169">2</a></span>
                            <span class="pagnLink"><a href="/s/ref=sr_pg_3?me=A1VZW46UOC1LOU&amp;rh=i%3Amerchant-items&amp;page=3&amp;ie=UTF8&amp;qid=1528764169">3</a></span>
                            <span class="pagnMore">...</span>
                            <span class="pagnDisabled">52</span>
                            <span class="pagnRA"> <a title="Nächste Seite" id="pagnNextLink" class="pagnNext" href="/s/ref=sr_pg_2?me=A1VZW46UOC1LOU&amp;rh=i%3Amerchant-items&amp;page=2&amp;ie=UTF8&amp;qid=1528764169">
                        <span id="pagnNextString">Nächste Seite</span>
                        <span class="srSprite pagnNextArrow"></span>
                     </a></span>
            <br clear="all">
        </div>
html;
        preg_match_all('/>\d+</', $html, $z);
        $z = array_map(function ($element){ return str_replace(['>', '<'], '', $element); }, $z[0]);
        Utils::debugView($z);
        return new Response('');

    }

    /**
     * @Route("/test997")
     * @return Response
     */
    public function pf ()
    {
//        $url = 'https://www.amazon.de/';
//        $url = 'https://www.amazon.de/s/?me=A1VZW46UOC1LOU&page=2';
        $url = 'https://2ip.ru';
        $parseVariant = 'html';
        $phantom = new \App\Core\Phantom($url, $parseVariant);
        $html = $phantom->getPhantomData();
        echo "<html>$html</html>";

        return new Response('');
    }

    /**
     * @Route("/test991")
     * @return Response
     */
    public function tt1 ()
    {
        $sellerId = 'A1VZW46UOC1LOU';
        $marketplaceId = 'A1PA6795UKMFR9';

        $manager = MongoManager::getInstance()->createManager();
        $z =
            $manager->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellersData')
                ->select(['totalCount', 'asins', 'pages'])
                ->field('asinProcessStatus')->equals(1)
                ->field('totalCount')->gte(100000)
                ->field('totalCount')->lte(10000000)
                ->getQuery()
                ->execute()
        ;
        $out = 0;
        $tc = 0;
        $ac = 0;
        foreach ($z as $c) {
            $asinsCount = count($c->getAsins());
            $totalCount = $c->getTotalCount();
//            $out += $totalCount/12;
            $tc += $totalCount;
            $ac += $asinsCount;
            $manager->clear();
        }
        Utils::debugView([$ac, $tc, round($ac/$tc, 2)*100]);
        return new Response($out/12);

    }




}
