<?php
namespace App\Core;

use App\Document\Items\Proxy;

/**
 * imported from old project
 * Class Parser
 * @package App\Core
 */
class Parser {

    /**
     * @var \DOMXPath
     */
    public $xpath_obj;

    /**
     * @var string ip:port
     */
    public $proxy;

    /**
     * @var string
     */
    private $user_agent;

    public function __construct()
    {
        $this->proxy_generate();
        $this->useragent_generate();
    }


    /**
     * @param $array
     * @return mixed
     */
    private function get_random_element($array)
    {
        return $array[mt_rand(0, count($array) - 1)];
    }

    private function useragent_generate()
    {
        $rnd1000 = mt_rand(1000, 2000);
        $rnd10 = mt_rand(11,99);
        $rnd1 = mt_rand(9,11);
        $useragent = [];
        $useragent[] = "Mozilla/5.0 Macintosh; Intel Mac OS X 10_10_3 AppleWebKit/{$rnd1000}.{$rnd10} KHTML, like Gecko Chrome/44.0.2376.0 Safari/537.36 OPR/31.0.1857.0";
        $useragent[] = "Mozilla/5.0 X11; Linux i686; rv:40.0 Gecko/{$rnd1000} Firefox/40.0";
        $useragent[] = "Mozilla/5.0 Windows NT 10.0; WOW64; rv:40.0 Gecko/{$rnd1000} Firefox/40.0";
        $useragent[] = "Mozilla/5.0 Macintosh; Intel Mac OS X 10_10_2 AppleWebKit/{$rnd1000}.{$rnd10} KHTML, like Gecko Chrome/40.0.2214.38 Safari/537.36";
        $useragent[] = "Mozilla/5.0 Windows NT 6.1; WOW64 AppleWebKit/{$rnd1000}.{$rnd10} KHTML, like Gecko Chrome/46.0.2490.71 Safari/537.36";
        $useragent[] = "Mozilla/5.0 Windows NT 6.1; WOW64 AppleWebKit/{$rnd1000}.{$rnd10} KHTML, like Gecko Chrome/51.0.2704.103 Safari/537.36";
        $useragent[] = "Mozilla/5.0 compatible; MSIE {$rnd1}.0; Windows NT 6.1; Trident/6.0";
        $useragent[] = "Mozilla/5.0 compatible; MSIE {$rnd1}.0; Windows NT 6.1; WOW64; Trident/6.0";
        $useragent[] = "Mozilla/5.0 compatible; MSIE {$rnd1}.0; Windows NT 6.2; Win64; x64; Trident/6.0";
        $useragent[] = "Mozilla/5.0 IE 11.0; Windows NT 6.3; Trident/7.0; .NET4.0E; .NET4.0C; rv:{$rnd1}.{$rnd10} like Gecko";

        $this->user_agent = $this->get_random_element($useragent);
    }

    /**
     * generate random proxy from list (file)
     */
    private function proxy_generate ()
    {
        $this->proxy = Proxy::randomProxy();
    }

    private function headers ()
    {
        return [
            'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'accept-encoding: gzip, deflate, br',
            'accept-language: en-US,en;q=0.9,ru;q=0.8',
            'cache-control: no-cache',
            //'cookie: session-id=260-6322716-0088940; x-wl-uid=1ILwa9hvlVnstmfKz3EjgU4mzie1O95x7qrxTndzEc4YJAxmPTgT47UNLsD4BP2CwNvUDqGvVXOY=; ubid-acbde=260-8199285-9767645; session-token="lOhBCayQNFa9i/FdVpqU+AOoZCyc6PEarvCX/5GyFnMLDK7WpikU/JVmVkhThDN+fGOGHjAfXxGpAlZNCuqXocjXjAScQHRr2Jg+6DpYiFzaEbqnM8qWzGDNzxJE7XDMnMsf1/UgkyM847u+qG7PlvO2KFjSy4JXzlXavQp1u/MFeavOVPFTov4/EWgqzrCIwfjETBe3RbMHMY3MTg95rqeEgRDRFELN9OT+/KuqcCrySFwF1ll6MKREFn8hjxGBOOOrbArEwI0="; lc-acbde=de_DE; session-id-time=2082787201l; csm-hit=tb:3A14ERKM81MENGJK9RGQ+s-W4JH08VK88HC3AB0PJYG|1529061668371&adb:adblk_no',
            'pragma: no-cache',
            'referer: https://www.amazon.de/',
            'upgrade-insecure-requests: 1',
            'user-agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36',
        ];
    }

    private function proxyAuth ()
    {
        return 'k_konstantin_n_gmail_com:c7R0NlEo';
    }

    /**
     * get full data of web page, include header, coockies etc
     * @param $url
     * @param string $cookies_in
     * @param bool $is_post
     * @param null $post_fields
     * @return mixed
     */
    public function webPageGet($url, $cookies_in = '', $is_post = false, $post_fields = null, $noProxy = false){
        $options = [
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => true,     //return headers in addition to content
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLINFO_HEADER_OUT    => true,
            CURLOPT_SSL_VERIFYPEER => false,     // Disabled SSL Cert checks
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_USERAGENT      => $this->user_agent,
            CURLOPT_COOKIE         => $cookies_in,
            CURLOPT_HTTPHEADER     => $this->headers(),
            CURLOPT_PROXYUSERPWD   => $this->proxyAuth(),

        ];
        if (!$noProxy) {
            $options[CURLOPT_PROXY] = $this->proxy;
        }

        if ($is_post) {
            $options[CURLOPT_POST] = 1;
            $options[CURLOPT_POSTFIELDS] = $post_fields;
        }

        $ch = curl_init( $url );
        curl_setopt_array( $ch, $options );
        $rough_content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );

        $header_content = substr($rough_content, 0, $header['header_size']);
        $body_content = trim(str_replace($header_content, '', $rough_content));
        $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
        preg_match_all($pattern, $header_content, $matches);
        $cookies_out = implode("; ", $matches['cookie']);

        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['headers']  = $header_content;
        $header['content'] = $body_content;
        $header['cookies'] = $cookies_out;
        return $header;
    }

    /**
     * @param $cookies_string
     * @return array
     */
    private function cookies_string_to_array ($cookies_string)
    {
        $cookies = explode(';', $cookies_string);
        $out = [];
        foreach ($cookies as $c) {
            $c = explode('=', trim($c));
            $out[$c[0]] = $c[1];
        }

        return $out;
    }

    /**
     * get URL - src attribute - of captcha image from captcha page
     * @param $html
     * @return bool|string
     */
    public function captcha_src_get ($html)
    {
        $html = explode('https://images-na.ssl-images-amazon.com/captcha/', $html);
        if (!isset($html[1])) return false;
        $captcha_source = explode('.jpg', $html[1])[0];
        return 'https://images-na.ssl-images-amazon.com/captcha/' . $captcha_source . '.jpg';
    }

    /**
     * fill xpath property
     * @param $html
     */
    public function xpath_create ($html)
    {
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);
        $this->xpath_obj = new \DOMXPath($dom);
    }

    /**
     * Get text content of single element of page, e.g. titel, manufacturer etc.
     * @param $xpath
     * @return string
     */
    public function text ($xpath)
    {
        $raw = $this->xpath_obj->query($xpath);
        if ($raw) {
            return trim($raw->item(0)->nodeValue);
        }
        return '';
    }

    /**
     * Get html of single element of page, e.g. titel, manufacturer etc.
     * @param $xpath
     * @return string
     */
    public function html ($xpath)
    {
        $node_element = $this->xpath_obj->query($xpath);
        if ($node_element->length && !is_null($node_element)) {
            return $this->inner_html($node_element->item(0));
        }
        return '';
    }

    /**
     * Получаем и склеиваем пробелами содержимое всех child nodes внутри HTML элемента
     * @param $xpath
     * @return string
     */
    public function full_text ($xpath)
    {
        $node_element = $this->xpath_obj->query($xpath);
        if ($node_element->length && !is_null($node_element)) {
            return $this->inner_text($node_element->item(0));
        }
        return '';
    }

    /**
     * get attribute number 2 of element
     * @param $xpath
     * @return string
     */
    public function attribute2 ($xpath)
    {
        $node_element = $this->xpath_obj->query($xpath);
        if ($node_element->length && !is_null($node_element)) {
            return $node_element->item(0)->attributes->item(2)->value;
        }
        return '';
    }

    /**
     * get attribute number 1 of element
     * @param $xpath
     * @return string
     */
    public function attribute1 ($xpath)
    {
        $node_element = $this->xpath_obj->query($xpath);
        if ($node_element->length && !is_null($node_element)) {
            return $node_element->item(0)->attributes->item(1)->value;
        }
        return '';
    }

    public function product_variables ($xpath)
    {
        $node_element = $this->xpath_obj->query($xpath);
        if ($node_element->length && !is_null($node_element)) {
            $block_html = $this->inner_html($node_element->item(0));
            preg_match_all('/dp\/[^\/]{5,20}\/ref/', $block_html, $z);
            if ($z[0] && is_array($z[0])) {
                $out = [];
                foreach ($z[0] as $raw) {
                    $out[] = str_replace(['dp/', '/ref'], '', $raw);
                }
                return implode(',', array_unique($out));
            }
        }
        return '';
    }


    /**
     * inner HTML of node
     * @param \DOMNode $element
     * @return string
     */
    private function inner_html (\DOMNode $element)
    {
        $inner_HTML = "";
        $children  = $element->childNodes;
        foreach ($children as $child)
        {
            $inner_HTML .= $element->ownerDocument->saveHTML($child);
        }
        return $inner_HTML;
    }

    /**
     * inner HTML of node
     * @param \DOMNode $element
     * @return string
     */
    private function inner_text (\DOMNode $element)
    {
        $inner_text = "";
        $children  = $element->childNodes;
        foreach ($children as $child)
        {
            $inner_text .= $element->ownerDocument->textContent . ' ';
        }
        return trim($inner_text);
    }
}