<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Core\Parsers\Amazon;

/**
 * Class SellerToAsinsParser
 * @package App\Core\Parsers\Amazon
 */
class SellerToAsinsParser
{
    public function productAjaxDataFields ($seller, $page = 1, $pageSize = 12, $marketplaceID = 'A1PA6795UKMFR9')
    {
        $url = "https://www.amazon.de/sp/hz/ajax/products";
        $product_search_request_data = [
            'marketplace' => $marketplaceID,
            'seller' => $seller,
            'url' => '/sp/hz/ajax/products',
            'pageSize' => $pageSize,
            'searchKeyword' => '',
            'extraRestrictions' => [],
            'pageNumber' => "$page",
        ];
        $product_search_request_data = json_encode($product_search_request_data);
        $data = [
            'marketplaceID' => $marketplaceID,
            'seller' => $seller,
            'productSearchRequestData' => urlencode($product_search_request_data)
        ];

        $post_fields = http_build_query($data);
        $post_fields = str_replace('%25', '%', $post_fields );
        $post_fields = str_replace('%5C', '', $post_fields );
        $post_fields = str_replace('%5B%5D', '%7B%7D', $post_fields );

        return $post_fields;
    }

    public function totalCount ($ajaxData)
    {
        return isset($ajaxData['viewData']['productsTotalCount'])?$ajaxData['viewData']['productsTotalCount']:false;
    }

    public function getAsins ($ajaxData) : array
    {
        $products = $ajaxData['products'];
        $out = [];
        if (is_array($products)) {
            foreach ($products as $p) {
                $url = $p['productUrlsMap']['products_tab'];
                $asin = explode('?', $url)[0];
                $asin = explode('dp/', $asin)[1];
                $out[] = $asin;
            }
        }

        return $out;
    }

    public function seller_url_generate ($seller, $marketplaceID = 'A1PA6795UKMFR9')
    {
        return "{$this->root_url}/sp?_encoding=UTF8&marketplaceID={$marketplaceID}&seller={$seller}";
    }


}
