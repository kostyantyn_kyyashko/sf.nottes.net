<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Core\Parsers\Amazon;

use App\Core\Parser;

class SellerFullParser_Prototype extends \App\Core\ParsedBase
{
    public $parser;

    public function __construct(Parser $parser, array $config = [])
    {
        $this->parser = $parser;
        parent::__construct($parser, $config);
    }

    public function url_generate (array $uniq_parameners)
    {
        $seller = $uniq_parameners['seller'];
        $marketplaceID = $uniq_parameners['marketplaceID'];
        return "{$this->root_url}/sp?_encoding=UTF8&marketplaceID={$marketplaceID}&seller={$seller}";
    }

    public static function urlForTotalCount ($sellerId, $marketplaceId = 'A1PA6795UKMFR9')
    {
        return "https://www.amazon.de/s?marketplaceID=$marketplaceId&merchant=$sellerId";
    }

    public function uniq_data_fields()
    {
        return [
            'seller',
            'marketplaceID',
        ];
    }

    public static function fields ($content = '')
    {
        return [
            'name' =>
                [
                    'handler' => 'text',
                    'xpath' => '//*[@id="sellerName"]',
                    'out' => trim($content),
                    'type' => 'string',
                    'index' => 'not_analyzed',
                ],
            'review_positiv' =>
                [
                    'handler' => 'text',
                    'xpath' => '//*[@id="seller-feedback-summary"]/span/a',
                    'out' => \App\Core\ParsedBase::value_from_string($content, 0),
                    'type' => 'float',
                ],
            'review_count' =>
                [
                    'handler' => 'text',
                    'xpath' => '//*[@id="seller-feedback-summary"]/span/a',
                    'out' => \app\components\ParsedBase::value_from_string($content, 2),
                    'type' => 'integer',
                ],
            'info' =>
                [
                    'handler' => 'html',
                    'xpath' => '//*[@id="about-seller-section"]',
                    'out' => self::remove_read_less($content),
                    'type' => 'string',
                    'index' => 'not_analyzed',
                ],
            'business_name' =>
                [
                    'handler' => 'text',
                    'xpath' => '//*[@id="seller-profile-container"]/div[2]/div/ul/li[1]/span/text()',
                    'out' => trim($content),
                    'type' => 'string',
                    'index' => 'not_analyzed',
                ],
            'business_type' =>
                [
                    'handler' => 'text',
                    'xpath' => '//*[@id="seller-profile-container"]/div[2]/div/ul/li[2]/span/text()',
                    'out' => trim($content),
                    'type' => 'string',
                    'index' => 'not_analyzed',
                ],
            'trade_register_number' =>
                [
                    'handler' => 'text',
                    'xpath' => '//*[@id="seller-profile-container"]/div[2]/div/ul/li[3]/span/text()',
                    'out' => trim($content),
                    'type' => 'string',
                    'index' => 'not_analyzed',
                ],
            'vat_number' =>
                [
                    'handler' => 'text',
                    'xpath' => '//*[@id="seller-profile-container"]/div[2]/div/ul/li[4]/span/text()',
                    'out' => trim($content),
                    'type' => 'string',
                    'index' => 'not_analyzed',
                ],
            'representative_name' =>
                [
                    'handler' => 'text',
                    'xpath' => '//*[@id="seller-profile-container"]/div[2]/div/ul/li[5]/span/text()',
                    'out' => trim($content),
                    'type' => 'string',
                    'index' => 'not_analyzed',
                ],
            'business_address' =>
                [
                    'handler' => 'html',
                    'xpath' => '//*[@id="seller-profile-container"]/div[2]/div/ul/li[6]/span/ul',
                    'out' => '<ul>' . trim($content) . '</ul>',
                    'type' => 'string',
                    'index' => 'not_analyzed',
                ],
            'review_block' =>
                [
                    'handler' => 'html',
                    'xpath' => '//*[@id="feedback-content"]',
                    'out' => trim($content),
                    'type' => 'string',
                    'index' => 'not_analyzed',
                ],
        ];

    }

    private function remove_read_less ($content)
    {
        return explode('<span class="a-declarative" data-action="reduce-text"', $content)[0];
    }


}
