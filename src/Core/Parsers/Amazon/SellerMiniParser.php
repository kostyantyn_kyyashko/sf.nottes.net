<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Core\Parsers\Amazon;

use App\Core\Parser;

/**
 * Class SellerMiniParser
 * @package App\Core\Parsers\Amazon
 */
class SellerMiniParser extends \App\Core\ParsedBase
{
    public $parser;

    public function __construct(Parser $parser, array $config = [])
    {
        $this->parser = $parser;
        parent::__construct($parser, $config);
    }

    public function url_generate (array $uniq_parameners)
    {
        $seller = $uniq_parameners['seller'];
        $marketplaceID = $uniq_parameners['marketplaceID'];
        return "{$this->root_url}/sp?_encoding=UTF8&marketplaceID={$marketplaceID}&seller={$seller}";
    }

    public function uniq_data_fields()
    {
        return [
            'seller',
            'marketplaceID',
        ];
    }

    public static function fields ($content = '')
    {
        return [
            'brand' =>
                [
                    'handler' => 'text',
                    'xpath' => '//*[@id="sellerName"]',
                    'out' => trim($content),
                ],
            'reviewPositive' =>
                [
                    'handler' => 'text',
                    'xpath' => '//*[@id="seller-feedback-summary"]/span/a',
                    'out' =>  \App\Core\ParsedBase::value_from_string($content, 0),
                    'type' => 'float',
                ],
            'reviewCount' =>
                [
                    'handler' => 'text',
                    'xpath' => '//*[@id="seller-feedback-summary"]/span/a',
                    'out' =>  \App\Core\ParsedBase::value_from_string($content, 2),
                    'type' => 'integer',
                ],
            'info' =>
                [
                    'handler' => 'html',
                    'xpath' => '//*[@id="about-seller-section"]',
                    'out' => trim($content),
                ],
            'detailed' =>
                [
                    'handler' => 'html',
                    'xpath' => '//*[@id="seller-profile-container"]/div[3]/div/ul',
                    'out' => trim($content),
                ],
            'reviewBlock' =>
                [
                    'handler' => 'html',
                    'xpath' => '//*[@id="feedback-content"]',
                    'out' => trim($content),
                ],
        ];

    }

    private function remove_read_less ($content)
    {
        return explode('<span class="a-declarative" data-action="reduce-text"', $content)[0];
    }


}
