<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Core\Parsers\Amazon;

/**
 * Class AsinMiniParser
 * @package App\Core\Parsers\Amazon
 */
class AsinMiniParser extends \App\Core\ParsedBase {

    public function __construct(\App\Core\Parser $parser, array $config = [])
    {
        parent::__construct($parser, $config = []);
    }


    /**
     * generate URL for product
     * @param $uniq_parameners
     * @return string
     */
    public function url_generate (array $uniq_parameners)
    {
        $asin = $uniq_parameners['asin'];
        return "{$this->root_url}/any/dp/$asin/";
    }
    public function uniq_data_fields()
    {
        return [
            'asin',
        ];
    }

    /**
     * main description for parser
     * description see below in first element
     * @return array
     */
    public static function fields ($content = '')
    {
        return [
            'title' => //element name of field, any - your choice
                [
                    'handler' => 'text', //handler
                    'xpath' => '//*[@id="productTitle"]',//xpath to element  //получаем текст в чистом виде
                    'out' => trim($content),
                ],
            'price' =>
                [
                    'handler' => 'text',
                    'xpath' => '//*[@id="olp_feature_div"]/div/span/span',//EUR 4.99
                    'out' => ($content),
                ],
            'price2' =>
                [
                    'handler' => 'text',
                    'xpath' => '//*[@id="priceblock_ourprice"]',//EUR 4.99
                    'out' => ($content),
                ],
            'qtyOffers' =>
                [
                    'handler' => 'text',
                    'xpath' => '//*[@id="olp_feature_div"]/div/span/a',//52&nbsp;new
                    'out' => ($content),
                ],
        ];
    }

    /**
     * @param $content
     * @return float
     */
    private static function price ($content)
    {
        $content = str_replace(',', '.', $content);
        $content = preg_replace('/[^\d\.]/', '', $content);
        return trim($content);
    }

    private static function qtyOffers ($content)
    {
        return intval(preg_replace('/^\d/', '', $content));
    }
}