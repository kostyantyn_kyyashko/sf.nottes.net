<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Core\Parsers\Amazon;

use App\Core\Parser;

/**
 * Class AsinToSellersParser
 * @package App\Core\Parsers\Amazon
 */
class AsinToSellersParser
{

    public $root_url = 'https://www.amazon.de';

    /**
     * generate URL for product
     * @param $asin
     * @param $start_index
     * @return string
     */
    public function urlGenerate($asin, $start_index=0)
    {
        return "{$this->root_url}/gp/offer-listing/$asin/?ie=UTF8&f_all=true&startIndex=$start_index";
    }

    public function isLastPage ($html)
    {
        if (strpos($html, 'olp_page_next')) {
            return false;
        }
        return true;
    }

    /**
     * return array of sellerID
     * @param $html
     * @return array
     */
    public function parseSeller ($html)
    {
        preg_match_all('/seller\=[A-Z0-9]{9,16}/', $html, $z);
        return array_map( function ($s)
            {return str_replace('seller=', '', $s);},
                array_keys(
                    array_flip($z[0]
                    )
                )
        );
    }

    public function get_title_and_qty_offers ($html)
    {
        $parser = new Parser();
        $parser->xpath_create($html);
        $title_xpath = '//*[@id="olpProductDetails"]/h1';
        $qty_offers_xpath = '//*[@id="olpProductDetails"]/h1';
        return [
            'title' => $parser->text($title_xpath),
            'qty_offers' => $parser->text($qty_offers_xpath),
            'time_stamp' => time(),
        ];
    }

}
