<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Core\Parsers\Amazon;

use App\Core\Parser;

/**
 * Class SellerMiniParser
 * @package App\Core\Parsers\Amazon
 */
class SellersDataParser extends \App\Core\ParsedBase
{
    public $parser;

    public function __construct(Parser $parser, array $config = [])
    {
        $this->parser = $parser;
        parent::__construct($parser, $config);
    }

    public function url_generate (array $uniq_parameters)
    {
        $sellerId = $uniq_parameters['sellerId'];
        $marketplaceID = $uniq_parameters['marketplaceId'];
        return "{$this->root_url}/sp?_encoding=UTF8&marketplaceID={$marketplaceID}&seller={$sellerId}";
    }

    public static function totalCountUrl ($sellerId, $marketplaceId = 'A1PA6795UKMFR9')
    {
        return "https://www.amazon.de/s?marketplaceID={$marketplaceId}&merchant={$sellerId}";
    }

    public function parseTotalCount ($html, $fromDelimiter = 'von') //von only for DE language
    {
        $pattern = '/\d+\sErgebnisse\sfür/';
        preg_match($pattern, $html, $z);
        if (count($z) > 0) {
            return preg_replace('/[^\d]/','', $z[0]);
        }
        $pattern = "/\d{1}\-{0,1}\d{0,3}\s$fromDelimiter\s[\d\.\,]{1,10}\s/";
        preg_match($pattern, $html, $z);
        if (count($z) > 0) {
            $out = explode(' ', trim($z[0]));
            $out = trim(end($out));
            $out = str_replace(['.', ','], '', $out);
            return intval($out);
        }
        return 0;
    }

    public function clearInfo ($html)
    {
        $r = explode('<span id="about-seller-truncated', $html);
        if (isset($r[1])) {
            $r = $r[1];
            $r = explode('</span>', $r);
            unset($r[0]);
            $r = implode('</span>', $r);
            $r = str_replace('</div>', '', $r);
            $r = preg_replace('/display:\s+none/', '', $r);
            return trim($r);
        }
        return $html;
    }

    public function uniq_data_fields()
    {
        return [
            'sellerId',
            'marketplaceId',
        ];
    }

    public static function fields ($content = '')
    {
        return [
            'sellerName' =>
                [
                    'handler' => 'text',
                    'xpath' => '//*[@id="sellerName"]',
                    'out' => trim($content),
                ],
            //info can placed in div with id "about-seller-section" (info1) or "about-seller" (info2)
            'info' =>
                [
                    'handler' => 'html',
                    'xpath' => '//*[@id="about-seller"]',
                    'out' => trim($content),
                ],
            'impressum' =>
                [
                    'handler' => 'html',
                    'xpath' => '//*[@id="seller-profile-container"]/div[3]/div/ul',
                    'out' => trim($content),
                ],
            'reviewPositive' =>
                [
                    'handler' => 'text',
                    'xpath' => '//*[@id="seller-feedback-summary"]/span/a',
                    'out' =>  \App\Core\ParsedBase::value_from_string($content, 0),
                ],
            'reviewCount' =>
                [
                    'handler' => 'text',
                    'xpath' => '//*[@id="seller-feedback-summary"]/span/a',
                    'out' =>  \App\Core\ParsedBase::value_from_string($content, 2),
                ],
            'reviewBlock' =>
                [
                    'handler' => 'html',
                    'xpath' => '//*[@id="feedback-content"]',
                    'out' => trim($content),
                ],
        ];

    }

    private function remove_read_less ($content)
    {
        return explode('<span class="a-declarative" data-action="reduce-text"', $content)[0];
    }


}
