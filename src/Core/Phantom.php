<?php
/**
 * Created by PhpStorm.
 * User: konst20
 * Date: 21.08.2017
 * Time: 4:58
 */

namespace App\Core;

use App\Document\Items\Proxy;
use App\Document\Utils;

class Phantom {

    /**
     * Адрес страницы, которую будем парсить
     * @var
     */
    private $url;

    /**
     * Что будем парсить
     * Ф-я с таким именем должна быть в файле /core/js/parse.js
     * @var
     */
    private $parseVariant;


    /**
     * корневая папка всех файлов работы phantomjs
     * @return string
     */
    private function phantomDirectory()
    {
        return Utils::getRootDir() . 'phantom';
    }

    /**
     * Здесь будут хранится куки, localstorage и cache для каждой сессий
     * @return string
     */
    private function storagePath ()
    {
        return $this->phantomDirectory() . '/storage';
    }

    private $sessionId;
    private $coockiePath;
    private $localstoragePath;
    private $cachePath;
    private $proxy;

    /**
     * Агрументы см. выше
     * Phantom constructor.
     * @param $url
     * @param $parseVariant
     */
    public function __construct($url, $parseVariant)
    {
        $this->url = $url;
        $this->parseVariant = $parseVariant;
        $this->generateSession();
        $this->generateProxy();
    }

    /**
     * папки для куки и пр.
     * @param $path
     */
    private function stotageDirectoryCreate($path)
    {
        mkdir($path);
        chmod($path, 0777);
    }

    /**
     * Создаем сессию, нужные пути и создаем папки
     */
    private function generateSession ()
    {
        $this->sessionId = md5(mt_rand(0, 1000000));
        $sessionPath = "{$this->storagePath()}/{$this->sessionId}";
        //$this->stotageDirectoryCreate($sessionPath);
        mkdir($sessionPath);
        chmod($sessionPath, 0777);


        $this->coockiePath = "{$sessionPath}/cookie";
        //$this->stotageDirectoryCreate($this->coockiePath);
        mkdir($this->coockiePath);
        chmod($this->coockiePath, 0777);


        $this->localstoragePath = "{$sessionPath}/localstorage";
        //$this->stotageDirectoryCreate($this->localstoragePath);
        mkdir($this->localstoragePath);
        chmod($this->localstoragePath, 0777);


        $this->cachePath = "{$sessionPath}/cache";
        //$this->stotageDirectoryCreate($this->cachePath);
        mkdir($this->cachePath);
        chmod($this->cachePath, 0777);

    }

    private static function delTree($dir) {
        $files = array_diff(scandir($dir), array('.','..'));
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? self::delTree("$dir/$file") : unlink("$dir/$file");
        }
        return rmdir($dir);
    }

    private function destroySession ()
    {

    }

    private function generateProxy ()
    {
        $this->proxy = Proxy::randomProxy();
    }

    /**
     * опции запуска PhantomJS, см. доки. В версии 2.1.1 SSL уже нормально работает, пинать фантома (пока) не нужно
     * @return string
     */
    private function phantomjsOptions()
    {
        $options = [
            "--ignore-ssl-errors=true",
            "--ssl-protocol=any",
            "--web-security=false",
            "--cookies-file={$this->coockiePath}/cookie.txt",
            "--disk-cache=true",
            "--disk-cache-path={$this->cachePath}",
            "--load-images=true",
            "--local-storage-path={$this->localstoragePath}",
            "--proxy={$this->proxy}",
            "--proxy-auth=k_konstantin_n_gmail_com:c7R0NlEo",
        ];
        return implode(' ', $options);
    }

    /**
     * Получение данных phantomjs
     * обработчик - ф-я JS в файле handler.js, parseVariant - имя этой ф-и
     * @return string
     */
    public function getPhantomData ()
    {
        $pathToJshandler = $this->phantomDirectory().'/js/handler.js';
        $pathToJQuery = $this->phantomDirectory().'/js/jquery.js';
        $exec_string = <<<txt
phantomjs {$this->phantomjsOptions()} {$pathToJshandler} "{$this->url}" {$this->parseVariant} {$pathToJQuery}
txt;
        return shell_exec($exec_string);
    }



}