<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Core\GearmanFunctions;

use App\Core\ParsedBase;
use App\Core\Parser;
use App\Core\Parsers\Amazon\AsinMiniParser;
use App\Document\Items\Proxy;
use App\Document\Utils;

class AsinBigData
{
    public static function Handler (\GearmanJob $job)
    {
        $job = unserialize($job->workload());
        $asin = $job['asin'];
        $marketplaceId = isset($job['marketplaceId'])?$job['marketplaceId']:'A1PA6795UKMFR9';
        echo "$asin\n";
        $proxy = new Proxy();
        $parser = new Parser();
        $ipProxy = Proxy::randomProxy();
        $parser->proxy = $ipProxy;
        $asinMiniParser = new AsinMiniParser($parser);

        $uniqParameters = [
            'asin' => $asin,
            'marketplaceId' => $marketplaceId,
        ];
        $proxy->increaseUsage($ipProxy);
        $data = $asinMiniParser->workParseMain($uniqParameters, $parser, $asinMiniParser);
        $title = $data['title'];
        $price = $data['price'];
        $price2 = $data['price2'];
        $qtyOffers = $data['qtyOffers'];
        $asinData = [
            'asin' => $asin,
            'marketplaceId' => $marketplaceId,
            'title' => $title,
            'price' => $price,
            'price2' => $price2,
            'qtyOffers' => $qtyOffers,
        ];

        $asinData['price'] = mb_strlen($asinData['price'])>2?$asinData['price']:$asinData['price2'];
        $asinData['qtyOffers'] = $asinData['qtyOffers']?$asinData['qtyOffers']:1;


        switch (ParsedBase::checkHtml($data['html'])) {
            case 'empty': //empty html
                $asinData['processStatus'] = 3;
                break;
            case 'amazon404':
                $asinData['processStatus'] = 404;
                break;
            case 'amazon500':
                $asinData['processStatus'] = 500;
                break;
            case 'captcha':
                $asinData['processStatus'] = 6;
                $proxy->increaseCaptcha($ipProxy);
                break;
            case 'success':
                if (strlen($title) > 2) {
                    $asinData['processStatus'] = 2;
                    $proxy->increaseSuccess($ipProxy);
                }
                else {
                    $asinData['processStatus'] = 3;
                }
                break;
        }

        $key = 'mfo304988kN';
        $url = "https://spider.sellerlogic.com/api/updateAsinData?key=$key";
        $parser = new Parser();
        echo $parser->webPageGet($url, '', 1, http_build_query($asinData), true)['content'] . "\n";
        return;
    }

    public static function WorkersDispatcher ()
    {
        $worker = Utils::clearClassName(__CLASS__) . '_Handler';

        $workersCount = 100;
        $gm = new \App\Core\GMonitor();
        if (Utils::getServerLoad() > 90) {
            $gm::workerStop($worker);
        }
        else {
            $delta = $workersCount - $gm->workerCount($worker);
            for ($i = 0; $i < $delta; $i++) {
                $gm::workerStart($worker);
            }
        }

        $gclient = new \GearmanClient();
        $gclient->addServer(\App\Core\GMonitor::$host, \App\Core\GMonitor::$port);
        $gclient->doBackground(Utils::clearClassName(__CLASS__) . '_' . __FUNCTION__, ' ');
        unset($gm);
        unset($gclient);
        sleep(mt_rand(1,5));
        return;

    }

    public static function JobsDispatcher ()
    {
        $functionName = Utils::clearClassName(__CLASS__) . '_Handler';
        $jobsCount = 120;
        $gm = new \App\Core\GMonitor();
        $gclient = new \GearmanClient();
        @$gclient->addServer($gm::$host, $gm::$port);
        $delta = $jobsCount - $gm->functionStatus($functionName);
        if ($delta > 0) {
            $url = "https://spider.sellerlogic.com/api/getAsins?delta=$delta";
            $asins = file_get_contents($url);
            $asins = json_decode($asins, 1);
            if ($asins && count($asins)) {
                foreach ($asins as $asin) {
                    $gearmanData = [
                        'asin' => $asin,
                        'marketplaceId' => 'A1PA6795UKMFR9',
                    ];
                    $gclient->doBackground($functionName, serialize($gearmanData));
                }
            }
        }

        $gclient->doBackground(Utils::clearClassName(__CLASS__) . '_' . __FUNCTION__, ' ');
        unset($gm);
        unset($gclient);
        return;

    }


}
