<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Core\GearmanFunctions;

use App\Document\MongoManager;

class TotalCount
{
    public static function handle ()
    {
        $job = [
            'sellerId' => 'A2P3P4T81ZT8KE',
            'marketplaceId' => 'A1PA6795UKMFR9',
        ];

        $sellerId = $job['sellerId'];
        $marketplaceId = isset($job['marketplaceId'])?$job['marketplaceId']:'A1PA6795UKMFR9';

        $gclient = new \GearmanClient();
        $gclient->addServer(\App\Core\GMonitor::$host, \App\Core\GMonitor::$port);

        $proxy = new \App\Document\Items\Proxy();
        $ipProxy = \App\Document\Items\Proxy::randomProxy();
        $proxy->increaseUsage($ipProxy);

        $parser = new \App\Core\Parser();
        $parser->proxy = $ipProxy;

        $sellerDataParser = new \App\Core\Parsers\Amazon\SellersDataParser($parser);

        $totalCountUrl = $sellerDataParser->totalCountUrl($sellerId, $marketplaceId);
//        echo $totalCountUrl . "\n";
        $html = $parser->webPageGet($totalCountUrl)['content'];
        $totalCount = $sellerDataParser->parseTotalCount($html, 'von\smehr\sals');
        if (!$totalCount) $totalCount = $sellerDataParser->parseTotalCount($html, 'von');
        $totalCount = str_replace(',', '', $totalCount);
        $totalCount = intval($totalCount);
        var_dump($totalCount);
        $manager = MongoManager::getInstance()->createManager();
        $manager->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellersData')
            ->updateOne()
            ->field('sellerId')->equals($sellerId)
            ->field('marketplaceId')->equals($marketplaceId)
            ->field('totalCount3')->set($totalCount)
            ->getQuery()
            ->execute();
    }
}
