<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Core\GearmanFunctions;

class ChainReaction
{
    /**
     * get page by asin, get asin's data and get sellerIDs
     * @param \GearmanJob $job
     */
    public static function getSellersByAsin (\GearmanJob $job)
    {
        $job = unserialize($job->workload());
        $asin = $job['asin'];
        $page = isset($job['page'])?$job['page']:1;

        $gclient = new \GearmanClient();
        $gclient->addServer(\App\Core\GMonitor::$host, \App\Core\GMonitor::$port);

        $asinToSellersParser = new \App\Core\Parsers\Amazon\AsinToSellersParser();
        $url = $asinToSellersParser->urlGenerate($asin, ($page - 1)*10);

        $proxy = new \App\Document\Items\Proxy();
        $ipProxy = \App\Document\Items\Proxy::randomProxy();
        $proxy->increaseUsage($ipProxy);

        $parser = new \App\Core\Parser();
        $parser->proxy = $ipProxy;
        $html = $parser->webPageGet($url)['content'];
        $sellers = $asinToSellersParser->parseSeller($html);

        //parse asin's page and save data
        if ($page == 1) {
            $asinMini = new \App\Document\Items\Amazon\AsinMini();
            $tmp = $asinMini->getter->findOneBy(['asin' => $asin]);
            if ($tmp && property_exists($tmp, 'title') && strlen($tmp->getTitle()) < 3) {//empty rec in DB. Clear that
                $mb = new \App\Document\MongoBase();
                $mb->remove($tmp);
                $tmp = false;
            }
            if(!$tmp) { //this asin not parsed and not saved in db
                $asinMiniParser = new \App\Core\Parsers\Amazon\AsinMiniParser($parser);
                $uniqData = [
                    'asin' => $asin,
                ];
                $asinData = $asinMiniParser->workParseMain($uniqData, $parser, $asinMiniParser);

                //echo "price {$asinData['price']}";

                if (array_key_exists('title', $asinData) && strlen($asinData['title']) > 3) {
                    $asinData['asin'] = $asin;
                    $asinMini->insertRow($asinData);
                }
            }
        }

        $sellersJsonBuffer = new \App\Document\Items\Amazon\SellersJsonBuffer();

        if ($sellers && is_array($sellers)) {
            $proxy->increaseSuccess($ipProxy);
            $sellersJsonBuffer->insertRow(['sellersJson' => json_encode($sellers)]);
        }
        if(!$asinToSellersParser->isLastPage($html)) {
            $gearman_data =
                [
                    'asin' => $asin,
                    'page' => $page+1
                ];
            $gclient->doBackground('GetSellersByAsin', serialize($gearman_data));
        }

        unset($gclient);
        return;
    }

    /**
     * get page by sellerID, get seller's data and get asins
     * @param \GearmanJob $job
     */
    public static function getAsinsBySeller (\GearmanJob $job)
    {
        $gclient = new \GearmanClient();
        $gclient->addServer(\App\Core\GMonitor::$host, \App\Core\GMonitor::$port);

        $job = unserialize($job->workload());
        $seller = $job['seller'];
        $page = isset($job['page'])?$job['page']:1;
        $sellerToAsinsParser = new \App\Core\Parsers\Amazon\SellerToAsinsParser();
        $postFields = $sellerToAsinsParser->productAjaxDataFields($seller, $page);

        $ajaxUrl = "https://www.amazon.de/sp/ajax/products";

        $proxy = new \App\Document\Items\Proxy();
        $ipProxy = \App\Document\Items\Proxy::randomProxy();
        $proxy->increaseUsage($ipProxy);

        $parser = new \App\Core\Parser();
        $parser->proxy = $ipProxy;
        $resp = $parser->webPageGet($ajaxUrl, '', 1, $postFields);
        $ajax_data = json_decode($resp['content'], 1);

        if (!$ajax_data) { //data not received - put task back to queue
            /*        $gearman_data = [
                        'seller' => $seller,
                        'page' => $page,
                    ];
                    $gclient->doBackground('GetAsinsBySeller', serialize($gearman_data));*/
        }
        else {

            $asinsJsonBuffer = new \App\Document\Items\Amazon\AsinsJsonBuffer();

            $pageSize = 12;

            $asins = $sellerToAsinsParser->getAsins($ajax_data);


            $total = $sellerToAsinsParser->totalCount($ajax_data);

            if ($asins && is_array($asins) && count($asins) > 0) { //print_r($asins); echo "\n\n$page\n\n";
                $proxy->increaseSuccess($ipProxy);
                $asinsJsonBuffer->insertRow(['asinsJson' => json_encode($asins)]);
            }

            if ($page == 1) {
                $sellerMini = new \App\Document\Items\Amazon\SellerMini();
                $tmp = $sellerMini->getter->findOneBy(['seller' => $seller]);
                if ($tmp && property_exists($tmp, 'info') && strlen($tmp->getInfo()) < 10) {//empty rec in DB. Clear that
                    $mb = new \App\Document\MongoBase();
                    $mb->remove($tmp);
                    $tmp = false;
                }
                if (!$tmp) {
                    $sellerMiniParser = new \App\Core\Parsers\Amazon\SellersDataParser($parser);
                    $uniq_data = [
                        'seller' => $seller,
                        'marketplaceID' => 'A1PA6795UKMFR9',
                    ];
                    $sellerData = $sellerMiniParser->workParseMain($uniq_data, $parser, $sellerMiniParser);
                    if (array_key_exists('info', $sellerData) && strlen($sellerData['info']) > 10) {//seller no empty
                        $sellerData['seller'] = $seller;
                        $sellerData['totalCount'] = $total;
                        $sellerMini->insertRow($sellerData);
                    }
                }
            }

            if ($pageSize * $page < $total) {
                $gearman_data = [
                    'seller' => $seller,
                    'page' => $page+1,
                ];
                $gclient->doBackground('GetAsinsBySeller', serialize($gearman_data));
            }

            unset($gclient);
            return;
        }
    }

    /**
     * main dispatcher. Check count of other dispatchers and restart it if need
     */
    public static function mainDispatcher ()
    {
        $disps = [
            'DispAsins' => 3,
            'DispSellers' => 15,
        ];

        $gm = new \App\Core\GMonitor();

        foreach ($disps as $disp => $count) {
            if (intval($gm->workerCount($disp)) < $count) {
                $gm::workerStart($disp);
            }
        }

        $gclient = new \GearmanClient();
        $gclient->addServer(\App\Core\GMonitor::$host, \App\Core\GMonitor::$port);
        $gclient->doBackground(__FUNCTION__, ' ');
        unset($gm);
        unset($gclient);
        sleep(mt_rand(1,5));
        return;
    }

    /**
     * asin's dispatcher
     */
    public static function asinsDispatcher ()
    {
        $workersCount = 81;
        $gm = new \App\Core\GMonitor();
        $gclient = new \GearmanClient();
        $gclient->addServer(\App\Core\GMonitor::$host, \App\Core\GMonitor::$port);
        $sellerMini = new \App\Document\Items\Amazon\SellerMini();
        $deltaAsins = intval($gm->functionStatus('GetAsinsBySeller'))
            - $workersCount;
        while ($deltaAsins < 0) {
            $deltaAsins = intval($gm->functionStatus('GetAsinsBySeller')) -
                $workersCount;

            $db = new \App\Document\Items\Amazon\SellersJsonBuffer();
            $items = $db->getAndRemoveItems();
            if ($items) {
                foreach ($items as $seller) {
                    $tmp = $sellerMini->getter->findOneBy(['seller' => $seller]);
                    if (!$tmp) { //this seller not parsed and not in DB
                        $gearman_data = [
                            'seller' => $seller,
                            'page' => 1,
                        ];
                        $gclient->doBackground('GetAsinsBySeller', serialize($gearman_data));
                    }
                }
            }
            //usleep(10000);
        }
        while (intval($gm->workerCount('GetAsinsBySeller')) < $workersCount) {
            \App\Core\GMonitor::workerStart('GetAsinsBySeller');
            usleep(10000);
        }
        usleep(mt_rand(100000, 500000));
        $gclient->doBackground(__FUNCTION__, serialize(' '));

        if ($gm->workerCount('Dsp') == 0) {
            \App\Core\GMonitor::workerStart('Dsp');
        }

        unset($gm);
        unset($gclient);
        return;
    }

    /**
     * seller's dispatcher
     */
    public static function sellersDispatcher ()
    {
        $workersCount = 81;
        $gm = new \App\Core\GMonitor();
        $gclient = new \GearmanClient();
        $gclient->addServer(\App\Core\GMonitor::$host, \App\Core\GMonitor::$port);

        $asinMini = new \App\Document\Items\Amazon\AsinMini();

        $deltaSellers = $gm->functionStatus('GetSellersByAsin')
            - $gm->workerCount('GetSellersByAsin');
        while ($deltaSellers < 0) {
            $deltaSellers = $gm->functionStatus('GetSellersByAsin')
                - $gm->workerCount('GetSellersByAsin');

            $db = new \App\Document\Items\Amazon\AsinsJsonBuffer();
            $items = $db->getAndRemoveItems();
            if ($items) {
                foreach ($items as $asin) {
                    $tmp = $asinMini->getter->findOneBy(['asin' => $asin]);
                    if (!$tmp) {
                        $gearman_data = [
                            'asin' => $asin,
                            'page' => 1,
                        ];
                        $gclient->doBackground('GetSellersByAsin', serialize($gearman_data));
                    }
                }
            }
            //usleep(10000);
        }

        while (intval($gm->workerCount('GetSellersByAsin')) < $workersCount) {
            \App\Core\GMonitor::workerStart('GetSellersByAsin');
            usleep(10000);
        }
        usleep(mt_rand(100000, 500000));
        $gclient->doBackground(__FUNCTION__, serialize(' '));

        if ($gm->workerCount('Dsp') == 0) {
            \App\Core\GMonitor::workerStart('Dsp');
        }

        unset($gm);
        unset($gclient);
        return;
    }
}
