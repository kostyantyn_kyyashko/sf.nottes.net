<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Core\GearmanFunctions;

use App\Document\Utils;

class AsinsBySellerBigDataPages
{
    public static function Handler (\GearmanJob $job)
    {
        $gclient = new \GearmanClient();
        $gclient->addServer(\App\Core\GMonitor::$host, \App\Core\GMonitor::$port);

        $job = unserialize($job->workload());
        $sellerId = $job['sellerId'];
        $page = isset($job['page'])?$job['page']:1;
        $marketplaceId = isset($job['marketplaceId'])?$job['marketplaceId']:'A1PA6795UKMFR9';

        $counter = isset($job['counter'])?$job['counter']:0;
        //count attempts of trying page data
        if ($counter > 5) return;

        $ajaxUrl = "https://www.amazon.de/sp/ajax/products";

        $pageSize = 12;

        $proxy = new \App\Document\Items\Proxy();
        $ipProxy = \App\Document\Items\Proxy::randomProxy();
        $parser = new \App\Core\Parser();
        $parser->proxy = $ipProxy;

        //get the count of seller's asins
        $totalCount = isset($job['totalCount'])?$job['totalCount']:false;
        if (!$totalCount) {
            $sellerDataParser = new \App\Core\Parsers\Amazon\SellersDataParser($parser);
            $totalCountUrl = $sellerDataParser->totalCountUrl($sellerId, $marketplaceId);

            $proxy->increaseUsage($ipProxy);
            $html = $parser->webPageGet($totalCountUrl)['content'];
            if ($html && strlen($html) > 1000) $proxy->increaseSuccess($ipProxy);

            $totalCount = $sellerDataParser->parseTotalCount($html, 'von\smehr\sals');
            if (!$totalCount) {
                $totalCount = $sellerDataParser->parseTotalCount($html, 'von');
            }
            $totalCount = str_replace(',', '', $totalCount);
            $totalCount = intval($totalCount);
        }

        $sellerToAsinsParser = new \App\Core\Parsers\Amazon\SellerToAsinsParser();
        $postFields = $sellerToAsinsParser->productAjaxDataFields($sellerId, $page, $pageSize);

        $proxy->increaseUsage($ipProxy);
        $resp = $parser->webPageGet($ajaxUrl, '', 1, $postFields)['content'];
        if ($resp && strlen($resp) > 1000) $proxy->increaseSuccess($ipProxy);

        $ajaxData = json_decode($resp, 1);
        $asins = $sellerToAsinsParser->getAsins($ajaxData);
        //some data not correct - put task back to queue, increase counter
        if (!($ajaxData && $totalCount && $asins && is_array($asins) && count($asins))) {
            $gearmanData = [
                'sellerId' => $sellerId,
                'page' => $page,
                'counter' => $counter+1,
            ];
            $gclient->doBackground(Utils::clearClassName(__CLASS__), serialize($gearmanData));
            return;
        }
        else {
            $manager = \App\Document\MongoManager::getInstance()->createManager();
            $asinMarketplaceId = new \App\Document\Items\Amazon\BigData\AsinMarkeplaceId();
            foreach ($asins as $asin) {
                //add asin to seller
                $manager
                    ->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellersData')
                    ->updateOne()
                    ->field('sellerId')->equals($sellerId)
                    ->field('marketplaceId')->equals($marketplaceId)
                    ->field('asins')->push($asin)
                    ->getQuery()
                    ->execute();
                //check if asin exists
                $asinDb = $asinMarketplaceId->getter->findOneBy([
                    'asin' => $asin,
                    'marketplaceId' => $marketplaceId
                ]);

                //no exists - insert to db
                if (!$asinDb) {
                    $asinMarketplaceId->insertRow([
                        'asin' => $asin,
                        'marketplaceId' => $marketplaceId,
                        'usage' => 1,
                        'sellerIds' => []
                    ]);
                }
                $manager
                    ->createQueryBuilder('\App\Document\Items\Amazon\BigData\AsinMarkeplaceId')
                    ->updateOne()
                    ->field('asin')->equals($asin)
                    ->field('marketplaceId')->equals($marketplaceId)
                    ->field('usage')->set(1)
                    ->field('sellerIds')->push($sellerId)
                    ->getQuery()
                    ->execute();
                //put page num
                $manager
                    ->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellersData')
                    ->updateOne()
                    ->field('sellerId')->equals($sellerId)
                    ->field('marketplaceId')->equals($marketplaceId)
                    ->field('pages')->push($page)
                    ->getQuery()
                    ->execute();

            }

            //not last page - put next page to queue
            if ($pageSize * $page <= $totalCount) {
                $gearmanData = [
                    'sellerId' => $sellerId,
                    'page' => $page+1,
                    'totalCount' => $totalCount
                ];
                $gclient->doBackground(Utils::clearClassName(__CLASS__) . '_' . __METHOD__ ,
                    serialize($gearmanData));
            }
            //last page - mark seller as handled
            else {
                $manager->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellersData')
                    ->updateOne()
                    ->field('sellerId')->equals($sellerId)
                    ->field('marketplaceId')->equals('A1PA6795UKMFR9')
                    ->field('asinProcessStatus')->set(2)
                    ->getQuery()
                    ->execute();

            }
            unset($gclient);
            return;
        }
    }

    public static function JobsDispatcher ()
    {
        $functionName = Utils::clearClassName(__CLASS__) . '_Handler';
        $jobsCount = 40;
        $gm = new \App\Core\GMonitor();
        $gclient = new \GearmanClient();
        $gclient->addServer(\App\Core\GMonitor::$host, \App\Core\GMonitor::$port);
        $delta = $jobsCount - $gm->functionStatus($functionName);
        $manager = \App\Document\MongoManager::getInstance()->createManager();
        for ($i = 0; $i < $delta; $i++) {
            $z = $manager->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellersData')
                ->field('asinProcessStatus')->equals(0)
                ->field('totalCount')->notEqual(0)
                ->field('sellerId')->equals(new \MongoRegex('/[A-Z0-9]{10,30}/'))
                ->limit(1)
                ->getQuery()
                ->execute();
            if (!$z->count()) break;
            $sellerId = array_values($z->toArray())[0]->getSellerId();
            $gearmanData = [
                'sellerId' => $sellerId,
                'marketplaceId' => 'A1PA6795UKMFR9',
                'page' => 1,
            ];
            $gclient->doBackground($functionName, serialize($gearmanData));
            //mark seller as in progress
            $manager->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellersData')
                ->updateOne()
                ->field('sellerId')->equals($sellerId)
                ->field('marketplaceId')->equals('A1PA6795UKMFR9')
                ->field('asinProcessStatus')->set(1)
                ->getQuery()
                ->execute();
        }

        $gclient->doBackground(Utils::clearClassName(__CLASS__) . '_' . __FUNCTION__, ' ');
        unset($gm);
        unset($gclient);
        sleep(mt_rand(1,5));
        return;
    }

    public static function WorkersDispatcher ()
    {
        $worker = Utils::clearClassName(__CLASS__) . '_Handler';

        $workersCount = 30;
        $gm = new \App\Core\GMonitor();
        if (Utils::getServerLoad() > 90) {
            //@todo workers stop
        }
        else {
            $delta = $workersCount - $gm->workerCount($worker);
            for ($i = 0; $i < $delta; $i++) {
                $gm::workerStart($worker);
            }
        }

        $gclient = new \GearmanClient();
        $gclient->addServer(\App\Core\GMonitor::$host, \App\Core\GMonitor::$port);
        $gclient->doBackground(Utils::clearClassName(__CLASS__) . '_' . __FUNCTION__, ' ');
        unset($gm);
        unset($gclient);
        sleep(mt_rand(1,5));
        return;
    }
}