<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Core\GearmanFunctions;

/**
 * get seller's data by sellerId from big Igor's file
 * Class SellersFullFromBigData
 * @package App\Core\GearmanFunctions
 */
class SellersFullFromBigData
{
    public static function handler (\GearmanJob $job)
    {
        $job = unserialize($job->workload());
        $sellerId = $job['sellerId'];
        $marketplaceId = isset($job['marketplaceId'])?$job['marketplaceId']:'A1PA6795UKMFR9';

        $gclient = new \GearmanClient();
        $gclient->addServer(\App\Core\GMonitor::$host, \App\Core\GMonitor::$port);

        $proxy = new \App\Document\Items\Proxy();
        $ipProxy = \App\Document\Items\Proxy::randomProxy();
        $proxy->increaseUsage($ipProxy);

        $parser = new \App\Core\Parser();
        $parser->proxy = $ipProxy;

        $sellerDataParser = new \App\Core\Parsers\Amazon\SellersDataParser($parser);

        $totalCountUrl = $sellerDataParser->totalCountUrl($sellerId, $marketplaceId);
        $html = $parser->webPageGet($totalCountUrl)['content'];
        $totalCount = $sellerDataParser->parseTotalCount($html, 'von\smehr\sals');
        if (!$totalCount) $totalCount = $sellerDataParser->parseTotalCount($html, 'von');
        $totalCount = str_replace(',', '', $totalCount);
        $totalCount = intval($totalCount);

        $uniqData = [
            'sellerId' => $sellerId,
            'marketplaceId' => $marketplaceId,
        ];
        $sellersParsedData = $sellerDataParser->workParseMain($uniqData, $parser, $sellerDataParser);

        echo $sellerId . "\n";

        if (array_key_exists('sellerName', $sellersParsedData) && strlen($sellersParsedData['sellerName']) > 3) {

            $proxy->increaseSuccess($ipProxy);

            $sellersData = new \App\Document\Items\Amazon\BigData\SellersData();
            $sellersParsedData['info'] = $sellerDataParser->clearInfo($sellersParsedData['info']);
            $sellersParsedData['sellerId'] = $sellerId;
            $sellersParsedData['marketplaceId'] = $marketplaceId;
            $sellersParsedData['totalCount'] = $totalCount;
            try {
                $sellersData->insertRow($sellersParsedData);
            }
            catch (\Exception $e) {}


            $manager = \App\Document\MongoManager::getInstance()->createManager();
            $manager->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellerIdMarkeplaceId')->findAndUpdate()
                ->field('sellerId')->equals($sellerId)
                ->field('marketplaceId')->equals($marketplaceId)
                ->field('isParsed')->set(2)
                ->getQuery()
                ->execute();
        }

        unset($parser);
        unset($proxy);
        unset($sellerDataParser);
        unset($sellersData);
        unset($manager);
        return;
    }

    public static function dispatcher ()
    {
        $workersCount = 100;
        $jobsCount = 500;
        $gm = new \App\Core\GMonitor();
        $gclient = new \GearmanClient();
        $gclient->addServer(\App\Core\GMonitor::$host, \App\Core\GMonitor::$port);


        $deltaSellers = $gm->functionStatus('SellersFullFromBigData')
            - $jobsCount;
        while ($deltaSellers < 0) {

            $deltaSellers = $gm->functionStatus('SellersFullFromBigData')
                - $jobsCount;

            //get sellerId+markertplaceId from DB
            $sellerIdMarketplaceId = new \App\Document\Items\Amazon\BigData\SellerIdMarkeplaceId();
            $data = $sellerIdMarketplaceId->getter
                ->findOneBy([
                    'isParsed' => 0,
                    'marketplaceId' => 'A1PA6795UKMFR9',
                ]);
            $sellerId = $data->getSellerId();
            $marketplaceId = $data->getMarketplaceId();

            //mark data 1 (in queue)
            $manager = \App\Document\MongoManager::getInstance()->createManager();
            $manager->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellerIdMarkeplaceId')->findAndUpdate()
                ->field('sellerId')->equals($sellerId)
                ->field('marketplaceId')->equals($marketplaceId)
                ->field('isParsed')->set(1)
                ->getQuery()
                ->execute();

            //put job to queue
            $queueData = [
                'sellerId' => $sellerId,
                'marketplaceId' => $marketplaceId,
            ];
            $gclient->doBackground('SellersFullFromBigData', serialize($queueData));

        }

        while (intval($gm->workerCount('SellersFullFromBigData')) < $workersCount) {
            \App\Core\GMonitor::workerStart('SellersFullFromBigData');
            usleep(10000);
        }
        usleep(mt_rand(100000, 500000));
        $gclient->doBackground(__FUNCTION__, serialize(' '));

        unset($gm);
        unset($gclient);
        unset($manager);
        unset($sellerIdMarketplaceId);
        return;
    }
}
