<?php
namespace App\Core;
use App\Document\Utils;
use function Symfony\Component\DependencyInjection\Loader\Configurator\expr;

/**
 * Class GMonitor
 * @package App\Core
 */
class GMonitor
{

    /**
     * Gearman host
     * default localhost
     * @var string
     */
    public static $host = '178.128.164.130';
    //public static $host = '127.0.0.1';

    /**
     * Gearman port
     */
    public static $port = 4730;


    private $timeout = 10;

    /**
     * @var bool|\resource
     */
    private $connection_handler = false;

    private $rootPath;

    /**
     * @throws \Exception
     * When the object is created immediately open a socket to the server,
     * failure - generated exception
     * Exception should be handled with each object creation
     *
     * The most common situation generate this exception is not running the server
     * or not connections to it
     */
    public function __construct($rootPath = ''){
        $this->rootPath = $rootPath;
        $this->connection_handler = @fsockopen(self::$host, self::$port, $errno, $errstr, $this->timeout);
        if(!$this->connection_handler){
            throw new \Exception("Error! Msg: " . $errstr . " ; Code: ". $errno);
        }
    }

    /**
     * close socket after work
     */
    public function __destruct(){
        if(is_resource($this->connection_handler)){
            fclose($this->connection_handler);
        }
    }

    /**
     * Send command to Gearman server
     * in this web-application use only commands 'status' and 'workers'
     * @param  $cmd
     * @return void
     */
    private function send_cmd($cmd){
        fwrite($this->connection_handler, $cmd."\r\n");
    }

    /**
     * Receive data from Gearman server after command send
     * @return string
     */
    private function receiveCmdData(){
        $full_response = '';
        while (true) {
            $data = fgets($this->connection_handler , 4096);
            if ($data == ".\n") {
                break;
            }
            $full_response .= $data;
        }

        return $full_response;
    }


    /**
     * This functions will added to Gearman Job Server
     * Must contain a some or all names of functions defined in /commands/functions.php
     * @return array
     */
    public static function functionsList()
    {
        $php = file_get_contents(Utils::getRootDir() . 'functions.php');
        preg_match_all('/function [^\(]+\s{0,5}\(/i', $php, $z);
        $out = [];
        if (isset($z[0]) && is_array($z[0])) {
            foreach ($z[0] as $raw_func) {
                $raw_func = str_replace('(', '', $raw_func);
                $raw_func = preg_replace('/function /', '', $raw_func);
                if ($raw_func[0] != '_') {
                    $out[] = trim($raw_func);
                }
            }
        }

        return $out;
    }

    /**
     * This functions will added to Gearman Job Server
     * Must contain a some or all names of functions defined in /commands/functions.php
     * @return array
     */
    public static function workerCommandsList()
    {
        $strict = true;
        if (!$strict) {
            ob_start();
            passthru("cd /var/www/spider.sellerlogic.com; php bin/console");
            $raw = ob_get_contents();
            ob_end_clean();
            $raw = explode('Available commands:', $raw)[1];
            $raw = explode('about', $raw)[0];
            $raw = explode(' ', trim($raw));
            $out = [];
            foreach ($raw as $r) {
                if (strlen($r) > 2 && $r != 'Fake') {
                    $out[] = $r;
                }
            }
            return array_filter($out);
        }
        else {
            return [
                'AsinBigData_Handler',
                'AsinBigData_WorkersDispatcher',
                'AsinBigData_JobsDispatcher',
            ];
        }
    }

    /**
     * @param string $functionName
     * @return int
     */
    public function workerCount ($functionName)
    {
/*        $command_string = 'ps ax | grep "' . $worker . '"';
        ob_start();
        system($command_string);
        $raw_out = ob_get_contents();
        ob_clean();
        return count(explode("\n", $raw_out)) - 3;*/

        $all_func_array = $this->allFunctionsStatuses();
        if (!array_key_exists('data', $all_func_array)) return false;
        if(!array_key_exists($functionName, $all_func_array['data'])){
            return false;
        }

        return $all_func_array['data'][$functionName]['capable_workers'];

    }

    /**
     * @param string $worker - 'main' or 'fake'
     * @return string
     */
    private static function workerCommandString ($worker)
    {
        return Utils::getRootDir() . "bin/console $worker ";
    }



    /**
     * The full list of functions that are registered on the server
     * Treatment done in the situation when the function was once registered
     * and Gearman responds that such a function is, but she 0 queue 0 in process and 0 for workers
     * @return array
     */
    public function allFunctionsStatuses ()
    {
        $this->send_cmd('status');
        $raw_data =  $this->receiveCmdData();

        $status = [
            'result' => 'fail',
        ];
        $temp_array = explode("\n", $raw_data);
        if (is_array($temp_array) && count($temp_array) > 0) {
            $status = [
                'result' => 'ok',
            ];
            foreach ($temp_array as $item) {
                $raw_array = explode("\t", $item);
                if(is_array($raw_array) && count($raw_array) == 4){
                    //this check need becase after stop of worker your function
                    //can be registered at Gearman Job server, this is a feature or fake )
                    if($raw_array[0] && ($raw_array[1] != 0 || $raw_array[2] !=0 || $raw_array[3] != 0)){
                        $status['data'][$raw_array[0]] = array(
                            'function_name' => $raw_array[0],
                            'in_queue' => $raw_array[1],
                            'jobs_running' => $raw_array[2],
                            'capable_workers' => $raw_array[3]
                        );
                    }

                }
            }

        }
        return $status;
    }

    /**
     * function status, false if none
     * @param  $function_name
     * @return mixed
     */
    public function functionStatus($function_name)
    {
        $all_func_array = $this->allFunctionsStatuses();
        if (!array_key_exists('data', $all_func_array)) return false;
        if(!array_key_exists($function_name, $all_func_array['data'])){
            return false;
        }

        return $all_func_array['data'][$function_name]['in_queue'];
    }

    /**
     * reset the task queue for the function
     * @param  $function_name
     * @return string
     */
    public function resetFunctionQueue($function_name)
    {
        $number_of_func = $this->functionStatus($function_name);
        if ($number_of_func > 0) {
            self::fakeWorkerStart($function_name);

            $counter = 1000;
            //wait until the queue is cleared
            while($number_of_func !=0 && $counter > 0){
                $number_of_func = $this->functionStatus($function_name);
                //small delay while Fake worker handle function queue
                usleep(10000);
                $counter--;
            }
            self::fakeWorkerStop();
            //queue is reset correctly
            if ($counter > 0) {
                return 'ok';
            }
            return 'fail';
        }
        return 'ok';
    }

    /**
     * Reset the entire queue - reset each task
     * @return string
     */
    public function resetAllQueue(){
        $functions_list = array_keys($this->allFunctionsStatuses()['data']);
        if (is_array($functions_list)) {
            foreach($functions_list as $function){
                //@todo check correctness of each function's reset
                $this->resetFunctionQueue($function);
            }
        }
        return 'ok';
    }

    /**
     * All workers, registered on Gearman server
     * This method not used and really not check
     * @todo check all data and functions as array
     * @return array
     */
    public function workersRegisteredOnGearman(){
        $this->send_cmd('workers');
        $raw_workers = $this->receiveCmdData();
        $workers = array();
        $temp_array = explode("\n", $raw_workers);
        foreach ($temp_array as $item) {

            $z = explode(" : ", $item);
            if(is_array($z) && count($z) > 1){
                $info = $z[0];
                $functions = $z[1];
                list($fd, $ip, $id) = explode(' ', $info);

                $functions = explode(' ', trim($functions));

                if(is_array($functions) && count($functions) > 0){
                    $workers[] = array(
                        'fd' => $fd,
                        'ip' => $ip,
                        'id' => $id,
                        'functions' => $functions
                    );
                }
            }
        }
        return $workers;
    }

    /**
     * Start worker
     * @param string $worker
     */
    public static function workerStart ($worker) {
        $command = "php " . self::workerCommandString($worker) ." > /dev/null &";
        exec($command);
    }

    /**
     * Stop workers
     * @param string $worker
     */
    public static function workerStop ($worker) {
        exec("ps ax | grep $worker | awk '{print $1}' | xargs kill");
    }

    /**
     * fake worker for reset $function queue
     * @param $function_name
     */
    public static function fakeWorkerStart($function_name) {
        exec("php " . self::workerCommandString('Fake') ." $function_name > /dev/null &");
    }

    /**
     * Stop fake worker
     */
    private static function fakeWorkerStop(){
        exec("ps ax | grep Fake | awk '{print $1}' | xargs kill");
    }
}
 
