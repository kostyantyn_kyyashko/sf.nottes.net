<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Core;

/**
 * imported from old project
 * Class ParsedBase
 * @package App\Core
 */
abstract class ParsedBase
{
    /**
     * object of Parser
     * @var Parser
     */
    public $parser;

    public $uniq_parameters;

    /**
     * Parsed constructor.
     * @param Parser $parser
     * @param array $config
     * @param array $uniq_parameters
     */
    public function __construct(Parser $parser, array $config = [], array $uniq_parameters = [])
    {
        $this->parser = $parser;
        $this->uniq_parameters = $uniq_parameters;
    }

    /**
     * Root URL of service
     * @var
     */
    public $root_url = 'https://www.amazon.de';

    /**
     * Generate URL by root URL and needed GET parameters of uniq data
     * @param array $uniq_parameners
     * @return mixed
     */
    abstract function url_generate(array $uniq_parameners);

    /**
     * Field of parser. Each field contains
     * @param string $content
     * @return array
     */
    abstract static function fields($content = '');

    /**
     * @return mixed
     */
    abstract function uniq_data_fields();

    /**
     * @param $key
     * @param $uniq_data
     * @param $parsed_object
     * @return mixed|string
     */
    public function  get_parsed_data ($key, $uniq_data, $parsed_object)
    {
        $handler = static::fields()[$key]['handler'];
        if ($handler != 'ajax') {
            $xpath = static::fields()[$key]['xpath'];
            $content = $this->parser->$handler($xpath);
            return static::fields($content)[$key]['out'];
        }
        else {
            $ajax_handler =  static::fields()[$key]['ajax_handler'];
            $ajax_data = isset(static::fields()[$key]['ajax_data'])?
                static::fields('')[$key]['ajax_handler']:null;
            if (method_exists(get_class($parsed_object), $ajax_handler)) {
                return call_user_func_array([get_class($parsed_object), $ajax_handler], [$ajax_data, $uniq_data]);
            }
        }
        return 'fail';
    }

    /**
     * @param $key
     * @return mixed
     */
    public function parse_fields_key ($key)
    {
        $handler = static::fields()[$key]['handler'];
        $xpath = static::fields()[$key]['xpath'];
        $content = $this->parser->$handler($xpath);
        return static::fields($content)[$key]['out'];
    }

    /**
     * @param $content
     * @param int $number
     * @return bool
     */
    public static function value_from_string ($content, $number = 0)
    {
        if ($content) {
            preg_match_all('/\d{1,10}\.{0,1}\d{0,4}/', $content, $out);
            $z =  array_values($out[0]);
            if (array_key_exists($number, $z)) return $z[$number];
        }

        return false;
    }

    /**
     * @param $uniqData
     * @param Parser $parser
     * @param ParsedBase $parsed_object
     * @return array
     */
    public function workParseMain($uniqData, \App\Core\Parser $parser, \App\Core\ParsedBase $parsed_object )
    {
        $cookies = $parser->webPageGet($parsed_object->root_url)['cookies'];
        //$cookies = '';
//        sleep(10);
        $url = $parsed_object->url_generate($uniqData);
        $page = $parser->webPageGet($url, $cookies);
        $html = $page['content'];
        $product_fields = array_keys($parsed_object::fields());
        $out = [];
        $parsed_object->parser->xpath_create($html);
        foreach ($product_fields as $key) {
            @$content = $parsed_object->get_parsed_data($key, $uniqData, $parsed_object);
            $out[$key] = $content;
        }
        $out['html'] = $html;
        return $out;
    }

    public static function checkHtml ($html) {
        if (strlen($html) < 100) return 'empty';
        if (stristr($html, 'captcha')) return 'captcha';
        if (stristr($html, 'Die eingegebene Webadresse')) return 'amazon404';
        if (stristr($html, 'ist ein technischer Fehler aufgetreten')) return 'amazon500';
        return 'success';
    }

}
