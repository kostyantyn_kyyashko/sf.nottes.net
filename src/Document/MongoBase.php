<?php
namespace App\Document;
class MongoBase
{
    public function __construct()
    {
        $this->manager = MongoManager::getInstance()->createManager();
        $this->getter = MongoManager::getInstance()->createGetter(get_class($this));
    }

    /**
     * @var \Doctrine\ODM\MongoDB\DocumentManager
     */
    public $manager;

    /**
     * @var \Doctrine\ODM\MongoDB\DocumentRepository
     */
    public $getter;


    /**
     * insert row to the "table"
     * array keys must comply property names of class
     * @param array $fieldValues [field1 => value1, field2 => value2, ...]
     */
    public function insertRow (array $fieldValues)
    {
        foreach ($fieldValues as $field => $value)
        {
            $class = get_class($this);
            if (property_exists($class, $field)) {
                $setter = 'set' . Utils::mb_ucfirst($field);
                $this->$setter($value);
            }
        }
        $this->save();
    }

    /**
     * save prepared row(s) to DB
     */
    public function save ()
    {
        $this->manager->persist($this);
        $this->manager->flush();
    }

    /**
     * remove prepared row's collection(s)
     * @param mixed  $obj
     */
    public function remove ($obj = false)
    {
        $obj = $obj?$obj:$this;
        $this->manager->remove($obj);
        $this->manager->flush();
    }

}
