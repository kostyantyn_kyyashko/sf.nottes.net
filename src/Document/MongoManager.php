<?php
namespace App\Document;
class MongoManager
{
    private function __construct() {}
    private function __clone() {}

    private static $instance = null;
    public static function getInstance ()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @return \Doctrine\Bundle\MongoDBBundle\ManagerRegistry
     */
    private function getMongoService ()
    {
        global $kernel;
        return $kernel->getContainer()->get('doctrine_mongodb');
    }

    /**
     * @return \Doctrine\ODM\MongoDB\DocumentManager
     */
    public function createManager ()
    {
        return $this->getMongoService()->getManager();
    }

    /**
     * @param string $nameSpace
     * @return \Doctrine\ODM\MongoDB\DocumentRepository
     */
    public function createGetter ($nameSpace)
    {
        return $this->getMongoService()->getRepository($nameSpace);
    }


}
