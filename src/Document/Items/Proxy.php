<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Document\Items;

use App\Document\MongoBase;
use App\Document\MongoManager;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class Proxy
 * @package App\Proxy
 * @MongoDB\Document(db="sf")
 */
class Proxy extends MongoBase
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @MongoDB\Id
     * @var int
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $ip;

    /**
     * @MongoDB\Field(type="int")
     * @var int
     */
    protected $usage = 0;

    /**
     * @MongoDB\Field(type="int")
     * @var int
     */
    protected $captcha = 0;

    /**
     * @MongoDB\Field(type="int")
     * @var int
     */
    protected $success = 0;

    /**
     * @MongoDB\Field(type="collection")
     * @var array
     */
    protected $test2;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return int
     */
    public function getUsage()
    {
        return $this->usage;
    }

    /**
     * @param int $usage
     */
    public function setUsage($usage)
    {
        $this->usage = $usage;
    }

    /**
     * @return int
     */
    public function getCaptcha()
    {
        return $this->captcha;
    }

    /**
     * @param int $captcha
     */
    public function setCaptcha($captcha)
    {
        $this->captcha = $captcha;
    }

    /**
     * @return int
     */
    public function getSuccess()
    {
        return $this->success;
    }

    /**
     * @param int $success
     */
    public function setSuccess($success)
    {
        $this->success = $success;
    }

    /**
     * @return array
     */
    public function getTest2()
    {
        return $this->test2;
    }

    /**
     * @param array $test2
     */
    public function setTest2($test2)
    {
        $this->test2 = $test2;
    }

    public function increaseUsage ($ip)
    {
        $proxy = $this->getter->findOneBy(['ip' => $ip]);
        if ($proxy) {
            $usage = $proxy->getUsage();
            $proxy->setUsage($usage + 1);
            $this->manager->persist($proxy);
            $this->manager->flush();
        }
    }

    public function increaseSuccess ($ip)
    {
        $proxy = $this->getter->findOneBy(['ip' => $ip]);
        if ($proxy) {
            $success = $proxy->getSuccess();
            $proxy->setSuccess($success + 1);
            $this->manager->persist($proxy);
            $this->manager->flush();
        }
    }

    public function increaseCaptcha ($ip)
    {
        $proxy = $this->getter->findOneBy(['ip' => $ip]);
        if ($proxy) {
            $success = $proxy->getCaptcha();
            $proxy->setCaptcha($success + 1);
            $this->manager->persist($proxy);
            $this->manager->flush();
        }
    }

    /**
     * @return string ip:port
     */
    public static function randomProxy ()
    {
        $getter = MongoManager::getInstance()->createGetter('App\Document\Items\Proxy');
        $proxies = $getter->findAll();
        $random = $proxies[mt_rand(0, count($proxies) - 1)]; // \App\Document\Items\Proxy
        return $random->getIp();
    }


}