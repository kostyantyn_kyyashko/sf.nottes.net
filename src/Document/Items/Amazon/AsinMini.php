<?php
namespace App\Document\Items\Amazon;

use App\Document\MongoBase;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class AsinMini
 * @MongoDB\Document(db="sf")
 */
class AsinMini extends MongoBase
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @MongoDB\Id
     * @var string
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $asin;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $title;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $price = 0;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $qtyOffers = 0;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAsin(): string
    {
        return $this->asin;
    }

    /**
     * @param string $asin
     */
    public function setAsin(string $asin)
    {
        $this->asin = $asin;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getPrice(): string
    {
        return $this->price;
    }

    /**
     * @param string $price
     */
    public function setPrice(string $price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getQtyOffers(): string
    {
        return $this->qtyOffers;
    }

    /**
     * @param string $qtyOffers
     */
    public function setQtyOffers(string $qtyOffers)
    {
        $this->qtyOffers = $qtyOffers;
    }

}
