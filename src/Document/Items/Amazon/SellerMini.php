<?php
namespace App\Document\Items\Amazon;

use App\Document\MongoBase;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class SellerMini
 * @MongoDB\Document(db="sf")
 */
class SellerMini extends MongoBase
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @MongoDB\Id
     * @var string
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $seller = '';

    /**
     * @MongoDB\Field(type="int")
     * @var int
     */
    protected $totalCount = '';

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $brand = '';

    /**
     * @MongoDB\Field(type="float")
     * @var float
     */
    protected $reviewPositive = '';

    /**
     * @MongoDB\Field(type="int")
     * @var int
     */
    protected $reviewCount = 0;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $info = '';

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $detailed = '';

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $reviewBlock = '';

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSeller(): string
    {
        return $this->seller;
    }

    /**
     * @param string $seller
     */
    public function setSeller(string $seller)
    {
        $this->seller = $seller;
    }

    /**
     * @return int
     */
    public function getTotalCount(): int
    {
        return $this->totalCount;
    }

    /**
     * @param int $totalCount
     */
    public function setTotalCount(int $totalCount)
    {
        $this->totalCount = $totalCount;
    }

    /**
     * @return string
     */
    public function getBrand(): string
    {
        return $this->brand;
    }

    /**
     * @param string $brand
     */
    public function setBrand(string $brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return float
     */
    public function getReviewPositive(): float
    {
        return $this->reviewPositive;
    }

    /**
     * @param float $reviewPositive
     */
    public function setReviewPositive(float $reviewPositive)
    {
        $this->reviewPositive = $reviewPositive;
    }

    /**
     * @return int
     */
    public function getReviewCount(): int
    {
        return $this->reviewCount;
    }

    /**
     * @param int $reviewCount
     */
    public function setReviewCount(int $reviewCount)
    {
        $this->reviewCount = $reviewCount;
    }

    /**
     * @return string
     */
    public function getInfo(): string
    {
        return $this->info;
    }

    /**
     * @param string $info
     */
    public function setInfo(string $info)
    {
        $this->info = $info;
    }

    /**
     * @return string
     */
    public function getDetailed(): string
    {
        return $this->detailed;
    }

    /**
     * @param string $detailed
     */
    public function setDetailed(string $detailed)
    {
        $this->detailed = $detailed;
    }

    /**
     * @return string
     */
    public function getReviewBlock(): string
    {
        return $this->reviewBlock;
    }

    /**
     * @param string $reviewBlock
     */
    public function setReviewBlock(string $reviewBlock)
    {
        $this->reviewBlock = $reviewBlock;
    }


}

