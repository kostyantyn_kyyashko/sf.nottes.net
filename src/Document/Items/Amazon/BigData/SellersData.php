<?php
namespace App\Document\Items\Amazon\BigData;

use App\Document\MongoBase;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class SellerFull
 * @MongoDB\Document
 * @MongoDB\Indexes({
 *     @MongoDB\Index(keys={"sellerId"="asc", "marketplaceId" = "asc"}, unique=true),
 *     })
 */
class SellersData extends MongoBase
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @MongoDB\Id
     * @var string
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string") @MongoDB\Index
     * @var string
     */
    protected $sellerId = '';

    /**
     * @MongoDB\Field(type="string") @MongoDB\Index
     * @var string
     */
    protected $marketplaceId = '';

    /**
     * @MongoDB\Field(type="string") @MongoDB\Index
     * @var string
     */
    protected $sellerName = '';

    /**
     * @MongoDB\Field(type="string") @MongoDB\Index
     * @var int
     */
    protected $totalCount = '';

    /**
     * @MongoDB\Field(type="int") @MongoDB\Index
     * @var int
     */
    protected $totalCount3 = 0;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $info = '';

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $impressum = '';

    /**
     * @MongoDB\Field(type="float") @MongoDB\Index
     * @var float
     */
    protected $reviewPositive = 0;

    /**
     * @MongoDB\Field(type="int") @MongoDB\Index
     * @var int
     */
    protected $reviewCount = 0;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $reviewBlock = '';

    /**
     * @MongoDB\Field(type="int") @MongoDB\Index
     * @var int (0 - not handled, 1 - asins obtaining now, 2 - all asins obtained)
     */
    protected $asinProcessStatus = 0;

    /**
     * @MongoDB\Field(type="collection")
     * @var array
     */
    protected $asins = [];

    /**
     * @MongoDB\Field(type="collection")
     * @var array
     */
    protected $pages = [];

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSellerId(): string
    {
        return $this->sellerId;
    }

    /**
     * @param string $sellerId
     */
    public function setSellerId(string $sellerId)
    {
        $this->sellerId = $sellerId;
    }

    /**
     * @return string
     */
    public function getMarketplaceId(): string
    {
        return $this->marketplaceId;
    }

    /**
     * @param string $marketplaceId
     */
    public function setMarketplaceId(string $marketplaceId)
    {
        $this->marketplaceId = $marketplaceId;
    }

    /**
     * @return string
     */
    public function getSellerName(): string
    {
        return $this->sellerName;
    }

    /**
     * @param string $sellerName
     */
    public function setSellerName(string $sellerName)
    {
        $this->sellerName = $sellerName;
    }

    /**
     * @return int
     */
    public function getTotalCount()
    {
        return $this->totalCount;
    }

    /**
     * @param int $totalCount
     */
    public function setTotalCount(int $totalCount)
    {
        $this->totalCount = $totalCount;
    }

    /**
     * @return string
     */
    public function getInfo(): string
    {
        return $this->info;
    }

    /**
     * @param string $info
     */
    public function setInfo(string $info)
    {
        $this->info = $info;
    }

    /**
     * @return string
     */
    public function getImpressum(): string
    {
        return $this->impressum;
    }

    /**
     * @param string $impressum
     */
    public function setImpressum(string $impressum)
    {
        $this->impressum = $impressum;
    }

    /**
     * @return float
     */
    public function getReviewPositive(): float
    {
        return $this->reviewPositive;
    }

    /**
     * @param float $reviewPositive
     */
    public function setReviewPositive(float $reviewPositive)
    {
        $this->reviewPositive = $reviewPositive;
    }

    /**
     * @return int
     */
    public function getReviewCount(): int
    {
        return $this->reviewCount;
    }

    /**
     * @param int $reviewCount
     */
    public function setReviewCount(int $reviewCount)
    {
        $this->reviewCount = $reviewCount;
    }

    /**
     * @return string
     */
    public function getReviewBlock(): string
    {
        return $this->reviewBlock;
    }

    /**
     * @param string $reviewBlock
     */
    public function setReviewBlock(string $reviewBlock)
    {
        $this->reviewBlock = $reviewBlock;
    }

    /**
     * @return int
     */
    public function getAsinProcessStatus(): int
    {
        return $this->asinProcessStatus;
    }

    /**
     * @param int $asinProcessStatus
     */
    public function setAsinProcessStatus(int $asinProcessStatus)
    {
        $this->asinProcessStatus = $asinProcessStatus;
    }

    /**
     * @return array
     */
    public function getAsins(): array
    {
        return $this->asins;
    }

    /**
     * @param array $asins
     */
    public function setAsins(array $asins)
    {
        $this->asins = $asins;
    }

    /**
     * @return array
     */
    public function getPages(): array
    {
        return $this->pages;
    }

    /**
     * @param array $pages
     */
    public function setPages(array $pages)
    {
        $this->pages = $pages;
    }



}

