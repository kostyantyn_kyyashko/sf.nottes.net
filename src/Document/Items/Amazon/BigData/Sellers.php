<?php
namespace App\Document\Items\Amazon\BigData;

use App\Document\MongoBase;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class Sellers
 * @MongoDB\Document(db="bigData")
 *   @MongoDB\Indexes({
 *     @MongoDB\Index(keys={"sellerId"="asc"}, unique=true),
 *     })
 */
class Sellers extends MongoBase
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @MongoDB\Id
     * @var string
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $sellerId;

    /**
     * @MongoDB\Field(type="int")
     * @var int
     */
    protected $isParsed;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSellerId(): string
    {
        return $this->sellerId;
    }

    /**
     * @param string $sellerId
     */
    public function setSellerId(string $sellerId)
    {
        $this->sellerId = $sellerId;
    }

    /**
     * @return int
     */
    public function getIsParsed(): int
    {
        return $this->isParsed;
    }

    /**
     * @param int $isParsed
     */
    public function setIsParsed(int $isParsed)
    {
        $this->isParsed = $isParsed;
    }

}
