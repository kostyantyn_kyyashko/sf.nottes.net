<?php
namespace App\Document\Items\Amazon\BigData;

use App\Document\MongoBase;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class AsinSellerIdMarkeplaceId
 * @MongoDB\Document(db="bigData")
 * @MongoDB\Indexes({
 *     @MongoDB\Index(keys={"asin"="asc"}),
 *     @MongoDB\Index(keys={"sellerId"="asc"}),
 *     @MongoDB\Index(keys={"marketplaceId"="asc"}),
 *     @MongoDB\Index(keys={"asin"="asc", "sellerId"="asc", "marketplaceId"="asc"}, unique=true)
 *     })
 */
class AsinSellerIdMarkeplaceId extends MongoBase
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @MongoDB\Id
     * @var string
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $asin;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $sellerId;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $marketplaceId;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAsin(): string
    {
        return $this->asin;
    }

    /**
     * @param string $asin
     */
    public function setAsin(string $asin)
    {
        $this->asin = $asin;
    }

    /**
     * @return string
     */
    public function getSellerId(): string
    {
        return $this->sellerId;
    }

    /**
     * @param string $sellerId
     */
    public function setSellerId(string $sellerId)
    {
        $this->sellerId = $sellerId;
    }

    /**
     * @return string
     */
    public function getMarketplaceId(): string
    {
        return $this->marketplaceId;
    }

    /**
     * @param string $marketplaceId
     */
    public function setMarketplaceId(string $marketplaceId)
    {
        $this->marketplaceId = $marketplaceId;
    }


}
