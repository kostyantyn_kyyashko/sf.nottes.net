<?php
namespace App\Document\Items\Amazon\BigData;

use App\Document\MongoBase;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class SellerIdMarkeplaceId
 * @MongoDB\Document(db="bigData")
 * @MongoDB\Indexes({
 *     @MongoDB\Index(keys={"sellerId"="asc"}),
 *     @MongoDB\Index(keys={"marketplaceId"="asc"}),
 *     @MongoDB\Index(keys={"sellerId"="asc", "marketplaceId"="asc"}, unique=true)
 *     })
 */
class SellerIdMarkeplaceId extends MongoBase
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @MongoDB\Id
     * @var string
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $sellerId;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $marketplaceId;

    /**
     * @MongoDB\Field(type="int")
     * @var int
     */
    protected $isParsed;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSellerId(): string
    {
        return $this->sellerId;
    }

    /**
     * @param string $sellerId
     */
    public function setSellerId(string $sellerId)
    {
        $this->sellerId = $sellerId;
    }

    /**
     * @return string
     */
    public function getMarketplaceId(): string
    {
        return $this->marketplaceId;
    }

    /**
     * @param string $marketplaceId
     */
    public function setMarketplaceId(string $marketplaceId)
    {
        $this->marketplaceId = $marketplaceId;
    }

    /**
     * @return int
     */
    public function getIsParsed(): int
    {
        return $this->isParsed;
    }

    /**
     * @param int $isParsed
     */
    public function setIsParsed(int $isParsed)
    {
        $this->isParsed = $isParsed;
    }




}
