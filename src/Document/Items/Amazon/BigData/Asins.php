<?php
namespace App\Document\Items\Amazon\BigData;

use App\Document\MongoBase;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class Asins
 * @MongoDB\Document(db="bigData")
 *   @MongoDB\Indexes({
 *     @MongoDB\Index(keys={"asin"="asc"}, unique=true),
 *     })
 */
class Asins extends MongoBase
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @MongoDB\Id
     * @var string
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $asin;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAsin(): string
    {
        return $this->asin;
    }

    /**
     * @param string $asin
     */
    public function setAsin(string $asin)
    {
        $this->asin = $asin;
    }

}
