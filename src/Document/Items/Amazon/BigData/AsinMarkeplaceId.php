<?php
namespace App\Document\Items\Amazon\BigData;

use App\Document\MongoBase;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class AsinMarkeplaceId
 * @MongoDB\Document(db="bigData")
 * @MongoDB\Indexes({
 *     @MongoDB\Index(keys={"asin"="asc"}),
 *     @MongoDB\Index(keys={"marketplaceId"="asc"}),
 *     @MongoDB\Index(keys={"usage"="asc"}),
 *     @MongoDB\Index(keys={"processStatus"="asc"}),
 *     @MongoDB\Index(keys={"title"="asc"}),
 *     @MongoDB\Index(keys={"price"="asc"}),
 *     @MongoDB\Index(keys={"qtyOffers"="asc"}),
 *     @MongoDB\Index(keys={"asin"="asc", "marketplaceId"="asc"}, unique=true)
 *     })
 */
class AsinMarkeplaceId extends MongoBase
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @MongoDB\Id
     * @var string
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $asin = '';

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $marketplaceId = '';

    /**
     * This asin is in our parsed sellers (0/1)
     * @MongoDB\Field(type="int")
     * @var int
     */
    protected $usage = 0;

    /**
     * 0 - not nandled/ 1 - in progress / 2 - finish
     * @MongoDB\Field(type="int")
     * @var int
     */
    protected $processStatus = 0;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $title = '';

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $price = 0;

    /**
     * This asin is in our parsed sellers (0/1)
     * @MongoDB\Field(type="int")
     * @var int
     */
    protected $qtyOffers = 0;

    /**
     * Seller IDs which have this asin
     * @MongoDB\Field(type="collection")
     * @var array
     */
    protected $sellerIds = [];

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAsin(): string
    {
        return $this->asin;
    }

    /**
     * @param string $asin
     */
    public function setAsin(string $asin)
    {
        $this->asin = $asin;
    }

    /**
     * @return string
     */
    public function getMarketplaceId(): string
    {
        return $this->marketplaceId;
    }

    /**
     * @param string $marketplaceId
     */
    public function setMarketplaceId(string $marketplaceId)
    {
        $this->marketplaceId = $marketplaceId;
    }
    /**
     * @return int
     */
    public function getUsage()
    {
        return $this->usage;
    }

    /**
     * @param int $usage
     */
    public function setUsage($usage)
    {
        $this->usage = $usage;
    }

    /**
     * @return array
     */
    public function getSellerIds(): array
    {
        return $this->sellerIds;
    }

    /**
     * @param array $sellerIds
     */
    public function setSellerIds(array $sellerIds)
    {
        $this->sellerIds = $sellerIds;
    }

    /**
     * @return int
     */
    public function getProcessStatus(): int
    {
        return $this->processStatus;
    }

    /**
     * @param int $processStatus
     */
    public function setProcessStatus(int $processStatus)
    {
        $this->processStatus = $processStatus;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price)
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getQtyOffers(): int
    {
        return $this->qtyOffers;
    }

    /**
     * @param int $qtyOffers
     */
    public function setQtyOffers(int $qtyOffers)
    {
        $this->qtyOffers = $qtyOffers;
    }



}
