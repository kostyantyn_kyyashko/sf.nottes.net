<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Document\Items\Amazon;

use App\Document\MongoBase;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * Class SellerToAsinsTmpBuffer
 * @MongoDB\Document(db="sf")
 */
class AsinsJsonBuffer extends MongoBase
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @MongoDB\Id
     * @var string
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $asinsJson = '';

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAsinsJson(): string
    {
        return $this->asinsJson;
    }

    /**
     * @param string $asinsJson
     */
    public function setAsinsJson(string $asinsJson)
    {
        $this->asinsJson = $asinsJson;
    }

    public function getAndRemoveItems ()
    {
        $buffer = $this->getter->findOneBy([]);
        if (!$buffer) return false;
        $items = $buffer->getAsinsJson();
        $items = json_decode($items, 1);
        $this->remove($buffer);
        return $items;
    }

}
