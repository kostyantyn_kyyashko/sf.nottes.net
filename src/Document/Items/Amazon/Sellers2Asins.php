<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Document\Items\Amazon;

use App\Document\MongoBase;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * data from Igor
 * Class Sellers2Asins
 * @MongoDB\Document(db="sf")
 */
class Sellers2Asins extends MongoBase
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @MongoDB\Id
     * @var string
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $seller_id = '';

    /**
     * @MongoDB\Field(type="int")
     * @var int
     */
    protected $parsed = 0;

    /**
     * @MongoDB\Field(type="collection")
     * @var array
     */
    protected $asins = [];

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSellerId(): string
    {
        return $this->seller_id;
    }

    /**
     * @param string $seller_id
     */
    public function setSellerId(string $seller_id)
    {
        $this->seller_id = $seller_id;
    }

    /**
     * @return int
     */
    public function getParsed(): int
    {
        return $this->parsed;
    }

    /**
     * @param int $parsed
     */
    public function setParsed(int $parsed)
    {
        $this->parsed = $parsed;
    }

    /**
     * @return string
     */
    public function getAsins(): string
    {
        return $this->asins;
    }

    /**
     * @param array $asins
     */
    public function setAsins(array $asins)
    {
        $this->asins = $asins;
    }



}
