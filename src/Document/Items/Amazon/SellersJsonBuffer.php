<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Document\Items\Amazon;

use App\Document\MongoBase;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class AsinToSellerTmpBuffer
 * @MongoDB\Document(db="sf")
 */
class SellersJsonBuffer extends MongoBase
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @MongoDB\Id
     * @var string
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $sellersJson = '';

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSellersJson(): string
    {
        return $this->sellersJson;
    }

    /**
     * @param string $sellersJson
     */
    public function setSellersJson(string $sellersJson)
    {
        $this->sellersJson = $sellersJson;
    }

    public function getAndRemoveItems ()
    {
        $buffer = $this->getter->findOneBy([]);
        if (!$buffer) return false;
        $items = $buffer->getSellersJson();
        $items = json_decode($items, 1);
        $this->remove($buffer);
        return $items;
    }



}
