<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Command;

use App\Document\Items\Amazon\BigData\AsinMarkeplaceId;
use App\Document\Items\Amazon\BigData\Asins;
use App\Document\Items\Amazon\BigData\AsinSellerIdMarkeplaceId;
use App\Document\Items\Amazon\BigData\SellerIdMarkeplaceId;
use App\Document\Items\Amazon\BigData\Sellers;
use App\Document\MongoBase;
use App\Document\Utils;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * mine data: sellerId, asin + marketplaceId from big dump file
 * Class BigDataFileHandler
 * @package App\Command
 */
class BigDataFileHandler extends Command
{
    private function pathToFile ()
    {
        return Utils::getRootDir() . 'src/_outerData/all-products.csv';
    }

    protected function configure()
    {
        $this->setName('BigDataFileHandler');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $handle = fopen($this->pathToFile(), "r");
        if ($handle) {
            $counter = 0;
            $startTime = time();

            while (($line = fgets($handle)) !== false) {

                $rawArray = explode(',', trim($line));
                if ($rawArray && count($rawArray) == 4) {
                    $asin = $rawArray[0];
                    $marketplaceId = $rawArray[1];
                    $sellerId = $rawArray[2];

                    $asinsDB = new Asins();
                    if (!$asinsDB->getter->findOneBy([
                        'asin' => $asin,
                    ])) {
                        $asinsDB->setAsin($asin);
                        $asinsDB->save();
                        $asinsDB->manager->clear();
                    }

                    $asinMarketplaceIdDB = new AsinMarkeplaceId();
                    if (!$asinMarketplaceIdDB->getter->findOneBy([
                        'asin' => $asin,
                        'marketplaceId' => $marketplaceId,
                        ])) {
                        $asinMarketplaceIdDB->setAsin($asin);
                        $asinMarketplaceIdDB->setMarketplaceId($marketplaceId);
                        $asinMarketplaceIdDB->save();
                        $asinMarketplaceIdDB->manager->clear();

                    }

                    $sellerIdDB = new Sellers();
                    if (!$sellerIdDB->getter->findOneBy([
                        'sellerId' => $sellerId,
                    ])) {
                        $sellerIdDB->setSellerId($sellerId);
                        $sellerIdDB->save();
                        $sellerIdDB->manager->clear();
                    }

                    $sellerIdmarketplaceIdDB = new SellerIdMarkeplaceId();
                    if (!$sellerIdmarketplaceIdDB->getter->findOneBy([
                        'sellerId' => $sellerId,
                        'marketplaceId' => $marketplaceId,
                    ])) {
                        $sellerIdmarketplaceIdDB->setSellerId($sellerId);
                        $sellerIdmarketplaceIdDB->setMarketplaceId($marketplaceId);
                        $sellerIdmarketplaceIdDB->save();
                        $sellerIdmarketplaceIdDB->manager->clear();
                    }

                    $asinSellerIdmarketplaceIdDB = new AsinSellerIdMarkeplaceId();
                    if (!$asinSellerIdmarketplaceIdDB->getter->findOneBy([
                        'asin' => $asin,
                        'sellerId' => $sellerId,
                        'marketplaceId' => $marketplaceId,
                    ])) {
                        $asinSellerIdmarketplaceIdDB->setAsin($asin);
                        $asinSellerIdmarketplaceIdDB->setSellerId($sellerId);
                        $asinSellerIdmarketplaceIdDB->setMarketplaceId($marketplaceId);
                        $asinSellerIdmarketplaceIdDB->save();
                        $asinSellerIdmarketplaceIdDB->manager->clear();
                    }

                    unset($asinsDB);
                    unset($asinMarketplaceIdDB);
                    unset($sellerIdDB);
                    unset($sellerIdmarketplaceIdDB);
                    unset($asinSellerIdmarketplaceIdDB);
                    unset($rawArray);
                }

                file_put_contents(Utils::getRootDir() . "public/import.log",
                    $counter . PHP_EOL);

                if ($counter%1000 == 0) {
                    $time = time() - $startTime;
                    $message = "$counter | $time sec";
                    echo "$message\n";
                    $startTime = time();
                }

                $counter++;
            }

            fclose($handle);
        }
    }}

