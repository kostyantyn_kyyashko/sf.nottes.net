<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Command;

use App\Core\GMonitor;
use App\Document\Utils;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class WorkerCommandTrait
 * @package App\Command
 */
trait WorkerCommandTrait
{
    protected $clearClassName;

    /**
     * @var \GearmanWorker
     */
    protected $worker;

    /**
     * for Command's constructor.
     */
    protected function init ()
    {
        $this->worker = new \GearmanWorker();
        @$this->worker->addServer(GMonitor::$host, GMonitor::$port);
        $this->clearClassName = Utils::clearClassName(__CLASS__);
    }

    /**
     * start Gearman worker with __CLASS__ function name
     */
    protected function createWorker ()
    {
        require_once Utils::getRootDir().'functions.php';
        $func = $this->clearClassName;
        if (is_callable($func)) {
            $this->worker->addFunction($func, $func);
        }
        while ($this->worker->work());
    }

    protected function configure()
    {
        $this->setName($this->clearClassName);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->createWorker();
    }

}
