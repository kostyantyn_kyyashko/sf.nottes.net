<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Command;

use App\Core\GMonitor;
use App\Document\Utils;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


/**
 * dispatcher for asins - view function DispAsins in <root directory>/functions.php
 * Class DispAsins
 * @package App\Command
 */
class DispAsins extends Command
{
    use WorkerCommandTrait;
    public function __construct($name = null)
    {
        $this->init();
        parent::__construct($name);
    }

    /**
     * Autostart function Dispatcher when it run - put task 'Dispatcher' on queue
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $gclient = new \GearmanClient();
        $gclient->addServer(GMonitor::$host, GMonitor::$port);
        $gclient->doBackground($this->clearClassName, ' ');
        unset($gclient);
    }
}

