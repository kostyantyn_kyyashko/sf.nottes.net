<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Command;

use App\Core\GMonitor;
use App\Document\Utils;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SellersFullFromBigData
 * @package App\Command
 */
class AsinsBySellerBigDataPages_JobsDispatcher extends Command
{
    use WorkerCommandTrait;
    public function __construct($name = null)
    {
        $this->init();
        parent::__construct($name);
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $gclient = new \GearmanClient();
        $gclient->addServer(GMonitor::$host, GMonitor::$port);
        $gclient->doBackground($this->clearClassName, ' ');
        unset($gclient);
    }


}
