<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Command;

use App\Document\Items\Amazon\BigData\AsinMarkeplaceId;
use App\Document\Items\Amazon\BigData\Asins;
use App\Document\Items\Amazon\BigData\AsinSellerIdMarkeplaceId;
use App\Document\Items\Amazon\BigData\SellerIdMarkeplaceId;
use App\Document\Items\Amazon\BigData\Sellers;
use App\Document\MongoBase;
use App\Document\Utils;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


/**
 * Sigle operation - add data from Igor's .xls file
 * Class Dispatcher
 * @package App\Command
 */
class XlsDataFileHandler extends Command
{
    private function pathToFile ()
    {
        return Utils::getRootDir() . 'src/_outerData/sellers.csv';
    }

    protected function configure()
    {
        $this->setName('XlsDataFileHandler');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $handle = fopen($this->pathToFile(), "r");
        if ($handle) {
            $counter = 0;
            $startTime = time();
            while (($line = fgets($handle)) !== false) {

                $marketplaceId = 'A1PA6795UKMFR9';
                $sellerId = trim($line);

                $sellerIdDB = new Sellers();
                if (!$sellerIdDB->getter->findOneBy([
                    'sellerId' => $sellerId,
                ])) {
                    $sellerIdDB->setSellerId($sellerId);
                    $sellerIdDB->setIsParsed(0);
                    $sellerIdDB->save();
                    $sellerIdDB->manager->clear();
                }

                $sellerIdmarketplaceIdDB = new SellerIdMarkeplaceId();
                if (!$sellerIdmarketplaceIdDB->getter->findOneBy([
                    'sellerId' => $sellerId,
                    'marketplaceId' => $marketplaceId,
                ])) {
                    $sellerIdmarketplaceIdDB->setSellerId($sellerId);
                    $sellerIdmarketplaceIdDB->setMarketplaceId($marketplaceId);
                    $sellerIdmarketplaceIdDB->save();
                    $sellerIdmarketplaceIdDB->manager->clear();
                }

                unset($sellerIdDB);
                unset($sellerIdmarketplaceIdDB);

                file_put_contents(Utils::getRootDir() . "public/import2.log",
                    $counter . PHP_EOL);

                if ($counter%1000 == 0) {
                    $time = time() - $startTime;
                    $message = "$counter | $time sec";
                    echo "$message\n";
                    $startTime = time();
                }

                $counter++;
            }

            fclose($handle);
        }
    }}

