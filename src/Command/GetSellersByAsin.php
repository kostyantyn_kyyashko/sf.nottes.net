<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Command;

use Symfony\Component\Console\Command\Command;

/**
 * view function GetSellersByAsin in <root directory>/functions.php
 * Class GetSellersByAsin
 * @package App\Command
 */
class GetSellersByAsin extends Command
{
    use WorkerCommandTrait;
    public function __construct($name = null)
    {
        $this->init();
        parent::__construct($name);
    }
}
