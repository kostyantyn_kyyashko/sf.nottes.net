<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Command;

use App\Core\GMonitor;
use App\Document\Utils;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * worker for reset queue
 * about: this worker create "empty" needed function and quickly handle tasks in queue
 * Class Fake
 * @package App\Command
 */
class Fake extends Command
{
    protected function configure()
    {
        $this->setName(Utils::clearClassName(__CLASS__))
            ->addArgument('functionName');
    }
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $functionName = $input->getArgument('functionName');
        eval('function ' . $functionName . '_fake(){};');
        $worker = new \GearmanWorker();
        $gm = new GMonitor();
        $worker->addServer(GMonitor::$host, GMonitor::$port);
        $worker->addFunction($functionName, $functionName . '_fake');
        while($worker->work()){
            if (!$gm->functionStatus($functionName)) die();
        }
    }
}
