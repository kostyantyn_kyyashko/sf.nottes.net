<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Command;

use App\Core\GearmanFunctions\TotalCount;
use App\Core\GMonitor;
use App\Document\MongoManager;
use App\Document\Utils;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


/**
 * dispatcher for sellers - view function Dsp in <root directory>/functions.php
 * Class Dispatcher
 * @package App\Command
 */
class LiveTestq extends Command
{

    protected function configure()
    {
        $this->setName('testq');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $manager = MongoManager::getInstance()->createManager();
        $manager->createQueryBuilder('\App\Document\Items\Amazon\BigData\SellersData')
            ->field('totalCount')->notEqual(0);
    }
}

