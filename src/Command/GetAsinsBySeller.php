<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Command;

use Symfony\Component\Console\Command\Command;

/**
 * view function GetAsinsBySeller in <root directory>/functions.php
 * Class GetAsinsBySeller
 * @package App\Command
 */
class GetAsinsBySeller extends Command
{
    use WorkerCommandTrait;
    public function __construct($name = null)
    {
        $this->init();
        parent::__construct($name);
    }

}
