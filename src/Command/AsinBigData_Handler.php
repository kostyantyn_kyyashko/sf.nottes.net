<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Command;

use Symfony\Component\Console\Command\Command;

/**
 * Class SellersFullFromBigData
 * @package App\Command
 */
class AsinBigData_Handler extends Command
{
    use WorkerCommandTrait;
    public function __construct($name = null)
    {
        $this->init();
        parent::__construct($name);
    }

}
